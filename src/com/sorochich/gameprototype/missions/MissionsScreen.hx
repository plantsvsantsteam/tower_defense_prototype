package com.sorochich.gameprototype.missions;
import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.events.ResourceEvent;
import com.sorochich.gameprototype.common.implementations.ScreenBase;
import com.sorochich.gameprototype.common.meta.I18n;
import com.sorochich.gameprototype.common.meta.Resources;
import haxe.ui.toolkit.containers.ListView;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.interfaces.IDisplayObject;
import com.sorochich.gameprototype.missions.MissionRenderer;
import openfl.events.MouseEvent;
import com.sorochich.gameprototype.common.const.Screen;
/**
 * ...
 * @author s_sorochich
 */
class MissionsScreen extends ScreenBase
{
	
	private var _view:IDisplayObject;
	private var _root:Root;
	private var _missionList:ListView;

	var _menuButton:Button;
	var _shopButton:Button;
	var _fightButton:Button;
	var _currencyText:Text;
	
	public function new() 
	{
		super();
	}
	
	override function onShow() 
	{
		_root = ApplicationMain.instance.get_uiRoot();
		_view = Toolkit.processXmlResource("layouts/missionsLayout.xml");
		_root.addChild(_view);
		_missionList = _root.findChild("missionsList", ListView, true);

		
		
		var ids = ApplicationMain.instance.get_globalConfig().getListOfMissionIds();
		for (id in ids){
		_missionList.dataSource.add({id:id}); 
		//_missionList.dataSource.add({id:"1"}); 
		//_missionList.dataSource.add({id:"2"}); 
		}	
		
		_missionList.setSelectedIndexNoEvent(0);
		
		super.onShow();
		_menuButton = _root.findChild("menuButton", Button, true);
		_shopButton = _root.findChild("shopButton", Button, true);
		_fightButton = _root.findChild("fightButton", Button, true);

		_menuButton.text = ApplicationMain.instance.get_textManager().getText(I18n.MENU_MENU_BUTTON);
		_shopButton.text = ApplicationMain.instance.get_textManager().getText(I18n.MENU_SHOP_BUTTON);
		_fightButton.text = ApplicationMain.instance.get_textManager().getText(I18n.MENU_FIGHT_BUTTON);
		
		_fightButton.addEventListener(MouseEvent.CLICK, fightButton_click);
		_shopButton.addEventListener(MouseEvent.CLICK, shopButton_click);
		_menuButton.addEventListener(MouseEvent.CLICK, menuButton_click);
		
		
		_currencyText = _root.findChild("curText", Text, true);
		
		_currencyText.style.color = 0xffffff;
		_currencyText.style.fontSize = 20;
		_currencyText.text = Std.string(ApplicationMain.instance.get_profileManager().getResourceValue(Resources.RESEARCH_POINTS)) + " R.P.";
		//trace(_currencyText.text );
		_currencyText.invalidate();
		ApplicationMain.instance.get_profileManager().addEventListener(ResourceEvent.RESOURCE_CHANGED, resourceChanged);
	}
	
	private function menuButton_click(e:MouseEvent):Void 
	{
		ApplicationMain.instance.get_screenManager().showScreen(Screen.MAIN_MENU);
		
	
	}
	private function fightButton_click(e:MouseEvent):Void 
	{
		var r:MissionRenderer = cast _missionList.selectedItems[0];
		ApplicationMain.instance.set_missionId(r.missionId);
		ApplicationMain.instance.get_screenManager().showScreen(Screen.BATTLE);
	}
	
	private function shopButton_click(e:MouseEvent):Void 
	{
		ApplicationMain.instance.get_screenManager().showScreen(Screen.SHOP);
	}
	private function resourceChanged(e:ResourceEvent):Void 
	{
		if(e.resId == Resources.RESEARCH_POINTS){
			_currencyText.text = Std.string(e.newCount) + " R.P.";
		}
	}
	
	override private function onHide() 
	{
		ApplicationMain.instance.get_profileManager().removeEventListener(ResourceEvent.RESOURCE_CHANGED, resourceChanged);
		_root.removeChild(_view);
		_view.dispose();

		 super.onHide();
		super.onHide();
	}
	
	
}