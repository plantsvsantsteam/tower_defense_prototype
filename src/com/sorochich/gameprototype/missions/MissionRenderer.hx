package com.sorochich.gameprototype.missions;

import com.sorochich.gameprototype.common.ApplicationMain;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.controls.Image;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.core.renderers.BasicItemRenderer;
import haxe.ui.toolkit.core.renderers.ItemRenderer;
import openfl.geom.Rectangle;
import com.sorochich.gameprototype.shop.ShopRendererLayout;

/**
 * ...
 * @author s_sorochich
 */
class MissionRenderer extends ItemRenderer
{
	private var _normalBack:Image = new Image();
	private var _ico:Image = new Image();
	private var _headerText:Text = new Text();
	private var _stars:Image = new Image();

	public var missionId:String;
	public function new() 
	{
		super();

		this.layout = new ShopRendererLayout();
		this.layout.padding = new Rectangle(0, 0, 0, 0);
		
		_normalBack.resource = "img/ui/missions/Mission_Back_active.png";
		//_normalBack.alpha = 0;
		//_ico.resource = "img/enemies/skin-1.png";
		_ico.x = 10;
		_ico.y = 5;
		//addChild(_normalBack);
		addChild(_ico);

		//style.backgroundImageRect = new Rectangle(0, 0, 256, 128);
		
		
		var holder:VBox = new VBox();
		holder.width = 252;
		holder.height = 85;
		//addChild(holder);
		//
		
		_headerText.x = 90;
		_headerText.y = 15;
		addChild(_headerText);
		
		
		
		_headerText.style.color = 0xffffff;
		_headerText.style.fontSize = 20;
		
		_stars.x = 120;
		_stars.y = 35;
		addChild(_stars);
		
		
	}
	
	override function set_data(value:Dynamic):Dynamic 
	{
		super.set_data(value);
		var vo = ApplicationMain.instance.get_globalConfig().getMissionById(Reflect.field(_data, "id"));
		_ico.resource = vo.missionIcon;
		missionId = vo.id;
		if(!ApplicationMain.instance.get_profileManager().isMissionUnlocked(vo.id)){
			_disabled = true;
			state = "disabled";
		}
		_headerText.text = "Mission: "+Std.string(Std.parseInt(vo.id)+1);
		var res:String = ApplicationMain.instance.get_profileManager().getMissionStatus(vo.id);
		if(res=="3"){
			_stars.resource = "img/ui/missions/Stars_BestResult.png";
		}
		if(res=="2"){
			_stars.resource = "img/ui/missions/Stars_GoodrResult.png";
		}
		if(res=="1"){
			_stars.resource = "img/ui/missions/Stars_SomeResult.png";
		}
		if(res=="0"){
			_stars.resource = "img/ui/missions/Stars_PoorResult.png";
		}
		if(res==null){
			_stars.resource = "img/ui/missions/Stars_NoResult.png";
		}
		//trace(this.);
		return value;
	}
	
}