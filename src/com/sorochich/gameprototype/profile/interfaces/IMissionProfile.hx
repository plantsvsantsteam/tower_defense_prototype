package com.sorochich.gameprototype.profile.interfaces;

/**
 * @author s_sorochich
 */
interface IMissionProfile 
{
	function isMissionUnlocked(id:String):Bool;
	
	function getMissionStatus(id:String):String;
	function saveMissionStatus(id:String, status:String):Void;
}