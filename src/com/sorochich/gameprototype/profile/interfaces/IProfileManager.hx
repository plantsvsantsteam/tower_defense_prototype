package com.sorochich.gameprototype.profile.interfaces;
import openfl.events.IEventDispatcher;

/**
 * @author s_sorochich
 */
interface IProfileManager extends IUserProfile extends IMissionProfile extends IEventDispatcher
{
	public function init(config:Xml):Void;
	
	public function getPrice(resId:String, curID:String):Int;
}