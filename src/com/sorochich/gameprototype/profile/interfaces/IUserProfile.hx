package com.sorochich.gameprototype.profile.interfaces;

/**
 * @author s_sorochich
 */
interface IUserProfile 
{
	public function contribute(resourceId:String, count:Int):Void;
	public function buy(resourceId:String, currencyId:String, count:UInt = 0):Void;
	public function getResourceValue(resourceId:String):UInt;
	
}