package com.sorochich.gameprototype.profile.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class ResourceVO
{

	private var price : Array<PriceVO>;
	public var id : String;
	public var name : String;
	public var startValue : UInt;

	public function new(xml:Fast) 
	{
		price = new Array<PriceVO>();
		id = xml.att.id;
		name = xml.att.name;
		startValue = Std.parseInt(xml.att.start_value);
		for (item in xml.nodes.price){
			price.push(new PriceVO(item));
		}
		
	}
	
	public function getPriceByCurId(id:String):PriceVO{
		for (p in price){
			if (p.currencyId == id){
				return p;
			}
		}
		return null;
	}
	
}