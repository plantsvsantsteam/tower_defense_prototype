package com.sorochich.gameprototype.profile.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class PriceVO
{

	public var currencyId: String;
	public var value:Int;
	
	public function new(xml:Fast) 
	{
		currencyId = xml.att.currencyId;
		value = Std.parseInt(xml.att.value);
		//trace(value);
	}
	
}