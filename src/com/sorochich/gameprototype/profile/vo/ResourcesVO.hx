package com.sorochich.gameprototype.profile.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class ResourcesVO
{
	private var resource : Array<ResourceVO>;

	public function new(xml:Fast) 
	{
		resource = new Array<ResourceVO>();
		for (item in xml.nodes.resource){
			resource.push(new ResourceVO(item));
		}
			
	}
	
	public function getResById(id:String):ResourceVO{
		for (res in resource){
			if (res.id == id){
				return res;
			}
		}
		return null;
	}
}