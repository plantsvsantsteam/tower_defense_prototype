package com.sorochich.gameprototype.profile.implementations;

import com.sorochich.gameprototype.common.meta.Resources;
import flash.errors.Error;
import haxe.xml.Fast;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;
import com.sorochich.gameprototype.profile.implementations.UserProfile;
import com.sorochich.gameprototype.profile.interfaces.IMissionProfile;
import com.sorochich.gameprototype.profile.interfaces.IProfileManager;
import com.sorochich.gameprototype.profile.interfaces.IUserProfile;
import com.sorochich.gameprototype.profile.vo.ResourcesVO;
import com.sorochich.gameprototype.profile.vo.UserResourcesVO;

/**
 * ...
 * @author s_sorochich
 */
class ProfileManager extends EventDispatcher implements IProfileManager
{
	private var _resourcesConfig:ResourcesVO;
	private var _profile:IUserProfile;
	private var _misionsProfile:IMissionProfile;

	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	
	/* INTERFACE profile.interfaces.IProfileManager */
	
	public function init(config:Xml):Void 
	{
		var fast:Fast = new Fast(config.firstElement());
		_resourcesConfig = new ResourcesVO(fast);
		_profile = new UserProfile(_resourcesConfig);
		_misionsProfile = new MissionProfile();
		
		//var res:UserResourcesVO = new UserResourcesVO();
		//trace(res.research_points);
		//trace(resConfig.getResById(Resources.SEED_0).getPriceByCurId(Resources.RESEARCH_POINTS).value);
	}
	
	
	/* INTERFACE profile.interfaces.IUserProfile */
	
	public function contribute(resourceId:String, count:Int):Void 
	{
		
		_profile.contribute(resourceId, count);
			
	}
	
	public function buy(resourceId:String, currencyId:String, count:UInt = 0):Void 
	{
		_profile.buy(resourceId, currencyId, count);
	}
	
	public function getResourceValue(resourceId:String):UInt 
	{
		return _profile.getResourceValue(resourceId);
	}
	
	
	/* INTERFACE profile.interfaces.IProfileManager */
	
	public function getPrice(resId:String, curID:String):Int 
	{
		return _resourcesConfig.getResById(resId).getPriceByCurId(curID).value;
	}
	
	public function isMissionUnlocked(id:String):Bool{
		
		return _misionsProfile.isMissionUnlocked(id);
	}
	
	public function getMissionStatus(id:String):String 
	{
		return _misionsProfile.getMissionStatus(id);
	}
	
	public function saveMissionStatus(id:String, status:String):Void 
	{
		_misionsProfile.saveMissionStatus(id,status);
	}
	
}