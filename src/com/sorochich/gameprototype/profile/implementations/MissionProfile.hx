package com.sorochich.gameprototype.profile.implementations;

import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.interfaces.IDataStorage;
import com.sorochich.gameprototype.profile.interfaces.IMissionProfile;

/**
 * ...
 * @author s_sorochich
 */
class MissionProfile implements IMissionProfile
{
	private var _dataStorage:IDataStorage;

	public function new() 
	{
		_dataStorage = ApplicationMain.instance.get_dataStorage();
	}
	
	
	/* INTERFACE profile.interfaces.IMissionProfile */
	
	public function isMissionUnlocked(id:String):Bool 
	{
		if (id == "0"){
			return true;
		}
		
		return getMissionStatus(Std.string(Std.parseInt(id) - 1)) != null;
	}
	
	
	/* INTERFACE profile.interfaces.IMissionProfile */
	
	public function getMissionStatus(id:String):String 
	{
		return _dataStorage.load("mission_" + id);
	}
	
	public function saveMissionStatus(id:String, status:String):Void 
	{
		trace(id, status);
		_dataStorage.save("mission_"+id, status);
	}
	
}