package com.sorochich.gameprototype.profile.implementations;

import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.events.ResourceEvent;
import com.sorochich.gameprototype.common.interfaces.IDataStorage;
import com.sorochich.gameprototype.common.meta.Resources;
import openfl.errors.Error;
import com.sorochich.gameprototype.profile.interfaces.IUserProfile;
import com.sorochich.gameprototype.profile.vo.PriceVO;
import com.sorochich.gameprototype.profile.vo.ResourcesVO;
import com.sorochich.gameprototype.profile.vo.UserResourcesVO;

/**
 * ...
 * @author s_sorochich
 */
class UserProfile implements IUserProfile
{

	private var _resources:UserResourcesVO = new UserResourcesVO();
	private var _resourcesConfig:ResourcesVO;
	var _dataStorage: IDataStorage;
	
	public function new(resourcesConfig:ResourcesVO) 
	{
		_resourcesConfig = resourcesConfig;
		_dataStorage = ApplicationMain.instance.get_dataStorage();
		initResourceValues();
		
	}
	
	private function initResourceValues():Void 
	{
		 
		for (res in Resources.ALL_RESOURCES){
			//trace('UserProfile | initResourceValues',);
			
			var value:Dynamic = _dataStorage.load(Resources.getResourceName(res));
			if (value == null){
				value = _resourcesConfig.getResById(res).startValue;			
			}
			
			Reflect.setField(_resources, Resources.getResourceName(res),value);
		}
	}
	
	
	/* INTERFACE profile.interfaces.IUserProfile */
	
	public function contribute(resourceId:String, count:Int):Void 
	{
		if (Resources.ALL_RESOURCES.indexOf(resourceId)!=-1){
			
			
			//_profile.contribute(resourceId, count);
			var resName:String = Resources.getResourceName(resourceId);
			var finalCount:UInt = Reflect.field(_resources, resName) + count;
			Reflect.setField(_resources, resName, finalCount);
			_dataStorage.save(resName, finalCount);
			ApplicationMain.instance.get_profileManager().dispatchEvent(new ResourceEvent(ResourceEvent.RESOURCE_CHANGED, resourceId, finalCount));
			trace("Contribute: ", resourceId, count, finalCount);
			//_resources
			
		}
		else{
			throw new Error("Trying to contribute not existing resource. Please check resourceId");
		}
	}
	
	public function buy(resourceId:String, currencyId:String, count:UInt = 0):Void 
	{
		if (Resources.ALL_RESOURCES.indexOf(resourceId)!=-1 && Resources.ALL_RESOURCES.indexOf(currencyId)!=-1){
			
			var priceVO: PriceVO = _resourcesConfig.getResById(resourceId).getPriceByCurId(currencyId);
			if (priceVO != null){
				var totalPrice:UInt = priceVO.value*count;
				if (totalPrice<=getResourceValue(currencyId)){
					contribute(resourceId, count);
					contribute(currencyId, -totalPrice);
					
				}else{	
					trace("Not enough of resource: ", currencyId);
					
				}
			}else{
				throw new Error("Try to buy resource using not allowed currency");
			}
			
		}
		else{
			throw new Error("Trying to contribute not existing resource or invalid currency. Please check resourceId");
		}
		
	}
	
	
	/* INTERFACE profile.interfaces.IUserProfile */
	
	public function getResourceValue(resourceId:String):UInt 
	{
		return Reflect.field(_resources, Resources.getResourceName(resourceId));
	}
	
}