package com.sorochich.gameprototype.toppanel;
import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.events.ResourceEvent;
import com.sorochich.gameprototype.common.meta.Resources;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.core.Component;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.interfaces.IDisplayObject;

/**
 * ...
 * @author s_sorochich
 */
class TopPanel
{

	private var _root:Root;
	private var _currencyText: Text;
	var _view:Component;
	public function new() 
	{
		//Toolkit.openPopup({x:0, y:0, percentWidth: 100, height: 50},function(root:Root) {

		_root = ApplicationMain.instance.get_uiRoot();
		_view = Toolkit.processXmlResource("layouts/topBar.xml");
		_view.disabled = true;
		_root.addChildAt(_view,0);
		_currencyText = _root.findChild("currencyText", Text, true);
		
		_currencyText.style.color = 0xffffff;
		_currencyText.style.fontSize = 20;
		_currencyText.text = Std.string(ApplicationMain.instance.get_profileManager().getResourceValue(Resources.RESEARCH_POINTS)) + "$";
		ApplicationMain.instance.get_profileManager().addEventListener(ResourceEvent.RESOURCE_CHANGED, resourceChanged);
		//shopBox = _root.findChild("myBox", VBox, true);
		//});
	}
	
	private function resourceChanged(e:ResourceEvent):Void 
	{
		if(e.resId == Resources.RESEARCH_POINTS){
			_currencyText.text = Std.string(e.newCount) + "$";
		}
	}
	
}