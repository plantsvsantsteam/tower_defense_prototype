package com.sorochich.gameprototype.macroses;

/**
 * ...
 * @author s_sorochich
 */
import haxe.macro.Context;
import haxe.macro.Expr;
import sys.io.File;

import sys.FileSystem;

class I18nGeneratorMacro {
  macro static public function build():Array<Field> {
	var filesList:Array<String> = FileSystem.readDirectory("assets/locales/en-US");
	var file:String;
	var tsvReg = ~/\.tsv/;
	var localeStrReg = ~/(\$\S*)\s+(.*)/;
	var fields = Context.getBuildFields();
	for (file in filesList){
		if (tsvReg.match(file)){
			var fileName:String = file.substring(0, file.length - 4);
			var fileContent:String = File.getContent("assets/locales/en-US/" + file);
			
			var strings:Array<String> = fileContent.split("\n");
			var str:String;
			for (str in strings){
				if(localeStrReg.match(str)){
					var varName:String = (fileName+'_'+localeStrReg.matched(1).substring(1, str.length-1)).toUpperCase();
					var varValue:String = (fileName+':'+localeStrReg.matched(1).substring(1, str.length-1));
					
					var newField = {
					  name: varName,
					  doc: null,
					  meta: [],
					  access: [AStatic, APublic, AInline],
					  kind: FVar(macro : String, macro $v{varValue}),
					  pos: Context.currentPos()
					};
					fields.push(newField);
				}
			}

		}
	}
    return fields;
  }
}