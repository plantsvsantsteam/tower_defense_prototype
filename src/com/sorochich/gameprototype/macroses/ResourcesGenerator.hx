package com.sorochich.gameprototype.macroses;

/**
 * ...
 * @author s_sorochich
 */
import haxe.ds.HashMap;
import haxe.macro.ComplexTypeTools;
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Expr.ComplexType;
import haxe.macro.ExprTools;
import haxe.xml.Fast;
import com.sorochich.gameprototype.macroses.BaseGenerator;
import sys.io.File;

class ResourcesGenerator {
  macro static public function build():Array<Field> {
	  
	var fileContent:String = File.getContent("assets/configs/resources.xml");
	trace(fileContent);
	var fields = Context.getBuildFields();
	var xml:Xml = Xml.parse(fileContent);
	var fast:Fast = new Fast(xml.firstElement());

	var allResourcesIds = [];
	var allResourcesNames = [];
	//var idToName:Map<String, String> = new Map<String, String>();
	
	
	for (item in fast.nodes.resource) {
		trace(item.att.name);
		var newField = BaseGenerator.createField(item.att.name.toUpperCase(),macro:String, macro $v{item.att.id},[AStatic, APublic, AInline]);
					fields.push(newField);
		allResourcesIds.push(item.att.id);
		allResourcesNames.push(item.att.name);
		//idToName[item.att.id] = item.att.name; 
	}
	

	fields.push( BaseGenerator.createField('ALL_RESOURCES', macro:Array<String>, macro $v{allResourcesIds},[AStatic, APublic]));
	fields.push( BaseGenerator.createField('ALL_RESOURCES_NAMES', macro:Array<String>, macro $v{allResourcesNames},[AStatic, APublic]));

    return fields;
  }

  
 


}