package com.sorochich.gameprototype.macroses;
import com.sorochich.gameprototype.battle.entitysystem.components.Position;
import com.sorochich.gameprototype.config.vo.Path;
import com.sorochich.gameprototype.macroses.SimplePoint;
import haxe.macro.Context;
import haxe.macro.Expr.Field;
import haxe.xml.Fast;
//import openfl.geom.Point;
import sys.io.File;

/**
 * ...
 * @author s_sorochich
 */
class PathsGenerator
{

	macro static public function build():Array<Field> {
	  
	var fileContent:String = File.getContent("assets/configs/paths.xml");
	trace(fileContent);
	var fields = Context.getBuildFields();
	var xml:Xml = Xml.parse(fileContent);
	var fast:Fast = new Fast(xml.firstElement());
//
	var paths:Array<Dynamic> = new Array<Dynamic>();
	//var allResourcesNames = [];
	////var idToName:Map<String, String> = new Map<String, String>();
	//
	//
	for (item in fast.nodes.path) {
		trace(item.att.id);
		var path:Dynamic = {};
		path.id = item.att.id;
		path.x = new Array<Float>();
		path.y = new Array<Float>();
		
		var splinePoint:Array<SimplePoint> = new Array<SimplePoint>();
		for (point in item.nodes.point) {
			//trace();
			var p:SimplePoint = new SimplePoint(Std.parseInt(point.att.x), Std.parseInt(point.att.y));
			splinePoint.push(p);
			
			
			//path.x.push(Std.parseInt(point.att.x));
			//path.y.push(Std.parseInt(point.att.y));
		}
		generatePath(path, splinePoint);
		paths.push(path);
		//var newField = BaseGenerator.createField(item.att.name.toUpperCase(),macro:String, macro $v{item.att.id},[AStatic, APublic, AInline]);
					//fields.push(newField);
		//allResourcesIds.push(item.att.id);
		//allResourcesNames.push(item.att.name);
		////idToName[item.att.id] = item.att.name; 
	}
	//
//
	fields.push( BaseGenerator.createField('pathsList', macro:Array<Dynamic>, macro $v{paths},[ APublic]));
	//fields.push( BaseGenerator.createField('ALL_RESOURCES_NAMES', macro:Array<String>, macro $v{allResourcesNames},[AStatic, APublic]));
//
    return fields;
  }
  
  static private function generatePath(path:Dynamic, Interpolation_Points:Array<SimplePoint>) 
  {
	  var Spline_Points:Array<SimplePoint> = new Array<SimplePoint>();
	  var distance:Int = 0;

	  for(k in  0 ... Interpolation_Points.length-3){ //loop through all the interpolation points, and build splines in between
		
		var point0:SimplePoint = Interpolation_Points[k];
		var point1:SimplePoint = Interpolation_Points[(k+1) % Interpolation_Points.length];
		
		var point2:SimplePoint = Interpolation_Points[(k+2) % Interpolation_Points.length];
		var point3:SimplePoint = Interpolation_Points[(k+3) % Interpolation_Points.length];

		var curD = Math.round(Math.sqrt ((point0.x-point1.x) * (point0.x-point1.x) + (point0.y-point1.y) * (point0.y-point1.y) ));
		var g:Float = 0.0; 
		
		while(g < 1.0){ //for each segment, calculate 200 spine points and store onto vector
			
			var setX:Array<Float> = calculate_catmull(g, point0.x, point1.x, point2.x, point3.x);
			var setY:Array<Float> = calculate_catmull(g, point0.y, point1.y, point2.y, point3.y);

			var spline_spot:SimplePoint = new SimplePoint(setX[0],setY[0]);
			var spline_spot_first:SimplePoint = new SimplePoint(setX[1],setY[1]);
			var spline_spot_second:SimplePoint = new SimplePoint(setX[2],setY[2]);
			//end incorrect labelling

			//trace(spline_spot);
			Spline_Points.push(spline_spot);
			path.x.push(spline_spot.x);
			path.y.push(spline_spot.y);
			g += 0.005;
		}
	}

	trace(Spline_Points.length);


	
  }
  
  private static function calculate_catmull(t:Float, p0:Float, p1:Float, p2:Float, p3:Float):Array<Float>{
	var t2:Float = t*t;
	var t3:Float = t2 * t;
	// P(t)
	var firstn:Float = (0.5 *( (2 * p1) + (-p0 + p2) * t +(2*p0 - 5*p1 + 4*p2 - p3) * t2 +(-p0 + 3*p1- 3*p2 + p3) * t3));
	// P'(t)
	var secondn:Float = 0.5 * (-p0 + p2) + t * (2*p0 - 5*p1 + 4*p2 - p3) + t2 * 1.5 * (-p0 + 3*p1 - 3*p2 + p3);
	// P''(t)
	var thirdn:Float = (2*p0 - 5*p1 + 4*p2 - p3) + t * 3.0 * (-p0 + 3*p1 - 3*p2 + p3);

	var arr:Array<Float> = [firstn, secondn, thirdn];
	return arr;
}
	
}