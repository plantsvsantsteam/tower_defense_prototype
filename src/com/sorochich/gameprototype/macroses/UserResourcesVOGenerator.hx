package com.sorochich.gameprototype.macroses;
import haxe.macro.Context;
import haxe.macro.Expr.Field;

import haxe.xml.Fast;
import sys.io.File;

/**
 * ...
 * @author s_sorochich
 */
class UserResourcesVOGenerator
{
	macro static public function build():Array<Field> {
	  
	var fileContent:String = File.getContent("assets/configs/resources.xml");
	trace(fileContent);
	var fields = Context.getBuildFields();
	var xml:Xml = Xml.parse(fileContent);
	var fast:Fast = new Fast(xml.firstElement());

	var allResources = [];
	for (item in fast.nodes.resource) {
		trace(item.att.name);
		var newField = BaseGenerator.createField(item.att.name,macro:UInt, macro 0,[APublic]);
					fields.push(newField);
		allResources.push(item.att.id);
	}
	
	//trace('[' + allResources.toString() + ']');
	//var f = BaseGenerator.createField('ALL_RESOURCES', macro:Array<String>, macro $v{allResources},[AStatic, APublic]);
	//trace(f);
	//fields.push(f);
    return fields;
  }
	
}