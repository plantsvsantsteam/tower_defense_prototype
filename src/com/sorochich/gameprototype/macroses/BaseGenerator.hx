package com.sorochich.gameprototype.macroses;
import haxe.macro.Context;
import haxe.macro.Expr.Field;

/**
 * ...
 * @author s_sorochich
 */
class BaseGenerator
{

	static public function createField(name, kind, value, access):Field 
	{
		return {
					name: name,
					doc: null,
					meta: [],
					access:access,
					//kind: FVar(macro : String, macro $v{varValue}),
					kind: FVar( kind, value),
					pos: Context.currentPos()
				  };
	}
	
}