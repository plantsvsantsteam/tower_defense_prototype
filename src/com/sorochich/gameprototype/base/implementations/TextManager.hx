package com.sorochich.gameprototype.base.implementations;

import com.sorochich.gameprototype.common.interfaces.ITextManager;
import firetongue.FireTongue;
import openfl.errors.Error;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;

/**
 * ...
 * @author s_sorochich
 */
class TextManager extends EventDispatcher implements ITextManager
{
	private var _tongue:FireTongue;
	private var _keyRegEx = ~/(\S*):(.*)/;
	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	
	/* INTERFACE common.interfaces.ITextManager */
	
	public function initialize(locale:String, callback:Void->Void = null):Void 
	{
		if (_tongue == null){
			_tongue  = new FireTongue();
			_tongue.init("en-US", callback, true);
		}else{
			throw new Error("TextManager is already initialized");
		}
	}
	
	public function getText(key:String): String
	{	
		if (_keyRegEx.match(key)){			
			return _tongue.get("$"+_keyRegEx.matched(2), _keyRegEx.matched(1));
		}else{
			throw new Error("Invalid key format");
		}
	}
	
}