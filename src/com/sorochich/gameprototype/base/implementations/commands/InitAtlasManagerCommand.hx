package com.sorochich.gameprototype.base.implementations.commands;

import com.sorochich.gameprototype.base.implementations.AtlasManager;
import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.interfaces.IAtlasManager;
import com.sorochich.gameprototype.common.interfaces.ICommand;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;

/**
 * ...
 * @author s_sorochich
 */
class InitAtlasManagerCommand extends EventDispatcher implements ICommand
{

	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	
	/* INTERFACE common.interfaces.ICommand */
	
	public function execute():Void 
	{
		trace("Initialize Atlas system");
		var m:IAtlasManager = new AtlasManager();
		m.init("img/enemies/enemies_spritesheet");
		ApplicationMain.instance.set_atlasManager(m);
		dispatchEvent(new Event(Event.COMPLETE));
	}
	
}