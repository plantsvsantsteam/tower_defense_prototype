package com.sorochich.gameprototype.base.implementations.commands;

import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.interfaces.ICommand;
import com.sorochich.gameprototype.config.implementations.GlobalConfig;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;

/**
 * ...
 * @author s_sorochich
 */
class InitGlobalConfig extends EventDispatcher implements ICommand
{

	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	
	/* INTERFACE common.interfaces.ICommand */
	
	public function execute():Void 
	{
		trace("Initialize GlobalConfig system");
		ApplicationMain.instance.set_globalConfig(new GlobalConfig());
		dispatchEvent(new Event(Event.COMPLETE));
	}
	
}