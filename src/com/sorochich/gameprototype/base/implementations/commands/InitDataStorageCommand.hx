package com.sorochich.gameprototype.base.implementations.commands;

import com.sorochich.gameprototype.base.implementations.DataStorage;
import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.interfaces.ICommand;
import com.sorochich.gameprototype.common.interfaces.IDataStorage;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;

/**
 * ...
 * @author s_sorochich
 */
class InitDataStorageCommand extends EventDispatcher implements ICommand
{

	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	
	/* INTERFACE common.interfaces.ICommand */
	
	public function execute():Void 
	{
		trace("Initialize DataStorage system");
		ApplicationMain.instance.set_dataStorage(new DataStorage());
		dispatchEvent(new Event(Event.COMPLETE));
	}
	
}