package com.sorochich.gameprototype.base.implementations.commands;

import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.interfaces.ICommand;
import openfl.Assets;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;
import com.sorochich.gameprototype.profile.implementations.ProfileManager;
import com.sorochich.gameprototype.profile.interfaces.IProfileManager;

/**
 * ...
 * @author s_sorochich
 */
class InitProfileManagerCommand extends EventDispatcher implements ICommand
{

	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	
	/* INTERFACE common.interfaces.ICommand */
	
	public function execute():Void 
	{
		trace("Initialize ProfileManager system");
		var profile: IProfileManager = new ProfileManager();
		profile.init(Xml.parse(Assets.getText("configs/resources.xml")));
		
		ApplicationMain.instance.set_profileManager(profile);
		dispatchEvent(new Event(Event.COMPLETE));
	}
	
}