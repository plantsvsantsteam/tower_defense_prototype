package com.sorochich.gameprototype.base.implementations.commands;

import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.interfaces.ICommand;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.themes.DefaultTheme;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;

/**
 * ...
 * @author s_sorochich
 */
class InitUIFrameworkCommand extends EventDispatcher implements ICommand
{

	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	
	/* INTERFACE common.interfaces.ICommand */
	
	public function execute():Void 
	{
		trace("Initialize UI system");
		Toolkit.theme = new DefaultTheme();
		Toolkit.init();
		Toolkit.openPopup({x:0, y:0, percentWidth: 100, percentHeight: 100},function(root:Root) {
			
			ApplicationMain.instance.set_uiRoot(root);
			dispatchEvent(new Event(Event.COMPLETE));
		});
		
	}
	
}