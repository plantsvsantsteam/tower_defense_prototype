package com.sorochich.gameprototype.base.implementations.commands;

import com.sorochich.gameprototype.base.implementations.ScreenManager;
import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.interfaces.ICommand;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;

/**
 * ...
 * @author s_sorochich
 */
class InitScreenManagerCommand extends EventDispatcher implements ICommand
{

	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	
	/* INTERFACE common.interfaces.ICommand */
	
	public function execute():Void 
	{
		trace("Initialize ScreenManager system");
		ApplicationMain.instance.set_screenManager(new ScreenManager());
		dispatchEvent(new Event(Event.COMPLETE));
		
	}
	
}