package com.sorochich.gameprototype.base.implementations.commands;

import com.sorochich.gameprototype.base.implementations.TextManager;
import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.interfaces.ICommand;
import openfl.events.Event;
import openfl.events.EventDispatcher;
/**
 * ...
 * @author s_sorochich
 */
class InitI18nCommand extends EventDispatcher implements ICommand
{

	private var _textManager: TextManager;
	
	public function new() 
	{
		super();
	}
	
	
	/* INTERFACE src.common.interfaces.ICommand */
	
	public function execute():Void 
	{
		trace("Initialize localization system");
		_textManager= new TextManager();
		_textManager.initialize("en-US", onFinish);
	}
	
	private function onFinish():Void 
	{
		ApplicationMain.instance.set_textManager(_textManager);
		dispatchEvent(new Event(Event.COMPLETE));
	}
	
}