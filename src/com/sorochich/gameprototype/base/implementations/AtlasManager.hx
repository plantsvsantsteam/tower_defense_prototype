package com.sorochich.gameprototype.base.implementations;

import com.sorochich.gameprototype.common.implementations.AtlasVO;
import com.sorochich.gameprototype.common.interfaces.IAtlasManager;
import openfl.display.DisplayObjectContainer;
import haxe.xml.Fast;
import openfl.Assets;
import openfl.display.Sprite;
import openfl.display.Tilesheet;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;
import openfl.geom.Rectangle;

/**
 * ...
 * @author s_sorochich
 */
class AtlasManager extends EventDispatcher implements IAtlasManager
{
	private var _items :Array<AtlasVO> = new Array<AtlasVO>();
	private var tileSheet:Tilesheet;
	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	
	/* INTERFACE common.interfaces.IAtlasManager */
	
	public function init(atlasName : String):Void
	{
		tileSheet = new Tilesheet(Assets.getBitmapData(atlasName+".png"));
		//trace(Assets.getText("img/enemies/enemy_0/enemy_0_run.xml"));
		var xml:Fast = new Fast(Xml.parse(Assets.getText(atlasName+".xml")).firstElement());
		
		var i:UInt = 0;
		for (node in xml.nodes.SubTexture){
			_items.push(new AtlasVO(node, i) );
			//trace(node.att.name);
			tileSheet.addTileRect(new Rectangle(Std.parseInt(node.att.x), Std.parseInt(node.att.y), Std.parseInt(node.att.width), Std.parseInt(node.att.height)));	
			i++;
		}
	}
	
	public function getItemsByPrefix(prefix:String):Array<AtlasVO>{
		var res:Array<AtlasVO> = new Array<AtlasVO>();
		
		for (item in _items){
			if(item.name.indexOf(prefix)!=-1){
				res.push(item);
			}
		}
		
		return res;

	}
	
	public function draw(container:Sprite, data:Array<Float>):Void{

		tileSheet.drawTiles ( container.graphics, data, true,Tilesheet.TILE_ROTATION);
	}
	
}