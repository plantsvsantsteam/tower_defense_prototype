package com.sorochich.gameprototype.base.implementations;

import com.sorochich.gameprototype.battle.BattleScreen;
import com.sorochich.gameprototype.common.const.Screen;
import com.sorochich.gameprototype.common.interfaces.IScreen;
import com.sorochich.gameprototype.common.interfaces.IScreenManager;
import com.sorochich.gameprototype.mainmenu.MainMenuScreen;
import com.sorochich.gameprototype.missions.MissionsScreen;
import openfl.errors.Error;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;
import com.sorochich.gameprototype.shop.ShopScreen;

/**
 * ...
 * @author s_sorochich
 */
class ScreenManager extends EventDispatcher implements IScreenManager
{
	private var _currentScreen:IScreen;

	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
		
	}
	
	
	/* INTERFACE common.interfaces.IScreenManager */
	
	public function showScreen(value:Screen):Void 
	{
		var newScreen:IScreen = getScreen(value);
		if(_currentScreen!=null){
			_currentScreen.hide();
		}
		_currentScreen = newScreen;
		_currentScreen.show();
	}
	
	private function getScreen(value:Screen):IScreen 
	{
		switch (value){
			case MAIN_MENU: return new MainMenuScreen();
			case PREBATTLE: return new MissionsScreen();
			case BATTLE: return new BattleScreen();
			case SHOP: return new com.sorochich.gameprototype.shop.ShopScreen();
		}
		
		throw new Error("Trying to invoke screen that not in list");
	}
	
}