package com.sorochich.gameprototype.base.implementations;

import com.sorochich.gameprototype.common.interfaces.IDataStorage;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;
import openfl.net.SharedObject;

/**
 * ...
 * @author s_sorochich
 */
class DataStorage extends EventDispatcher implements IDataStorage
{

	private var _so:SharedObject;
	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		_so = SharedObject.getLocal("TowerDefenseApp");
		
	}
	
	
	/* INTERFACE common.interfaces.IDataStorage */
	
	public function load(dataId:String):Dynamic 
	{
		return Reflect.field(_so.data, dataId);
	}
	
	public function save(dataId:String, data:Dynamic):Void 
	{
		Reflect.setField(_so.data, dataId,data);
		_so.flush();
	}
	
}