package com.sorochich.gameprototype.shop;

import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.meta.I18n;
import com.sorochich.gameprototype.common.meta.Resources;
import com.sorochich.gameprototype.config.vo.TowerVO;
import firetongue.Replace;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Image;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.core.Component;
import openfl.events.MouseEvent;
import openfl.text.TextFormatAlign;

/**
 * ...
 * @author s_sorochich
 */
class ShopPanel extends Component
{
	private var _normalBack:Image = new Image();
	private var _towerIcon:Image = new Image();
	private var _towerVO:TowerVO;
	
	private var _headerText:Text = new Text();
	
	private var _reloadingDescription:Text = new Text();
	private var _reloadingValue:Text = new Text();
	
	private var _damageDescription:Text = new Text();
	private var _damageValue:Text = new Text();
	
	private var _growthDescription:Text = new Text();
	private var _growthValue:Text = new Text();

	private var _buyButton:Button = new Button();
	
	public function new() 
	{
		super();
		this.layout = new ShopRendererLayout();
		//trace(this.layout);
		
		_normalBack.resource = "img/ui/shop/Shop_back.png";
		addChild(_normalBack);
		addChild(_towerIcon);
		_towerIcon.x = 190;
		_towerIcon.y = 105;
		
		_headerText.y = 230;
		_headerText.x = 210;

		addChild(_headerText);

		_headerText.style.color = 0xffffff;
		_headerText.style.fontSize = 20;
		_headerText.style.textAlign = TextFormatAlign.CENTER;
		
		
		_reloadingDescription.y = 260;
		_reloadingDescription.x = 150;

		addChild(_reloadingDescription);

		_reloadingDescription.style.color = 0xffffff;
		_reloadingDescription.style.fontSize = 14;
		_reloadingDescription.style.textAlign = TextFormatAlign.LEFT;
		
		_reloadingValue.y = 260;
		_reloadingValue.x = 350;

		addChild(_reloadingValue);

		_reloadingValue.style.color = 0xffffff;
		_reloadingValue.style.fontSize = 14;
		_reloadingValue.style.textAlign = TextFormatAlign.RIGHT;
		
		
		_damageDescription.y = 290;
		_damageDescription.x = 150;

		addChild(_damageDescription);

		_damageDescription.style.color = 0xffffff;
		_damageDescription.style.fontSize = 14;
		_damageDescription.style.textAlign = TextFormatAlign.LEFT;
		
		_damageValue.y = 290;
		_damageValue.x = 350;

		addChild(_damageValue);

		_damageValue.style.color = 0xffffff;
		_damageValue.style.fontSize = 14;
		_damageValue.style.textAlign = TextFormatAlign.RIGHT;
		
		
		_growthDescription.y = 320;
		_growthDescription.x = 150;

		addChild(_growthDescription);

		_growthDescription.style.color = 0xffffff;
		_growthDescription.style.fontSize = 14;
		_growthDescription.style.textAlign = TextFormatAlign.LEFT;
		
		_growthValue.y = 320;
		_growthValue.x = 350;

		addChild(_growthValue);

		_growthValue.style.color = 0xffffff;
		_growthValue.style.fontSize = 14;
		_growthValue.style.textAlign = TextFormatAlign.RIGHT;
		
		
		addChild(_buyButton);
		_buyButton.set_autoSize(false);
		_buyButton.autoSize = false;
		_buyButton.width = 136;
		_buyButton.height = 61;
		_buyButton.x = 200;
		_buyButton.y = 370;
		//_buyButton.
		_buyButton.text = ApplicationMain.instance.get_textManager().getText(I18n.SHOP_BUY_BUTTON);
		
		trace(_buyButton.style.fontEmbedded);
		trace(_buyButton.style.fontName);
		trace(_buyButton.style.fontScale);
		_buyButton.addEventListener(MouseEvent.CLICK, buyButton_click);
	}
	
	private function buyButton_click(e:MouseEvent):Void 
	{
		ApplicationMain.instance.get_profileManager().buy(_towerVO.resourceId, Resources.RESEARCH_POINTS, 1);
	}
	
	public function setTowerId(currentId:String) 
	{
		_towerVO = ApplicationMain.instance.get_globalConfig().getTowerById(currentId);
		
		_towerIcon.resource = _towerVO.assets.active;
		
		_headerText.text = ApplicationMain.instance.get_textManager().getText(_towerVO.name);
		
		_reloadingDescription.text = ApplicationMain.instance.get_textManager().getText(I18n.SHOP_RELOADING_DESCR);
		_reloadingValue.text =  Replace.flags(ApplicationMain.instance.get_textManager().getText(I18n.SHOP_RELOADING_VALUE), ["<value>"], [Std.string(_towerVO.gun.reloadingTime)]);
		
		_damageDescription.text = ApplicationMain.instance.get_textManager().getText(I18n.SHOP_DAMAGE_DESCR);
		_damageValue.text =Std.string(ApplicationMain.instance.get_globalConfig().getBulletById(_towerVO.gun.bulletId).damage);
		
		
		_growthDescription.text = ApplicationMain.instance.get_textManager().getText(I18n.SHOP_GROWTH_DESCR);
		_growthValue.text =  Replace.flags(ApplicationMain.instance.get_textManager().getText(I18n.SHOP_GROWTH_VALUE), ["<value>"], [Std.string(_towerVO.growthTime)]);
	}
	
	
	override public function dispose():Void 
	{
		_buyButton.removeEventListener(MouseEvent.CLICK, buyButton_click);
		
		super.dispose();
	}
	
}