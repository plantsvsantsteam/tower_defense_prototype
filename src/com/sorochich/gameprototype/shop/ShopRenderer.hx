package com.sorochich.gameprototype.shop;

import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.events.ResourceEvent;
import com.sorochich.gameprototype.common.meta.I18n;
import com.sorochich.gameprototype.common.meta.Resources;
import com.sorochich.gameprototype.config.vo.TowerVO;
import firetongue.Replace;
import haxe.ui.toolkit.controls.Image;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.core.Component;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.events.Event;
import openfl.events.MouseEvent;

/**
 * ...
 * @author s_sorochich
 */
class ShopRenderer extends Component
{

	private var _towerVO:TowerVO;
	private var _normalBack:Image = new Image();
	private var _activeBack:Image = new Image();
	private var _icon:Image =new Image();
	var towerId:String;
	
	private var _headerText:Text = new Text();
	private var _amountText:Text = new Text();
	private var _priceText:Text = new Text();
	var isBattle:Bool;
	@:isVar
	private var selected(get, set):Bool;
	
	public function new(towerId:String=null, isBattle:Bool = false) 
	//public function new() 
	{
		super();
		this.isBattle = isBattle;
		this._autoSize = false;
		this.layout = new ShopRendererLayout();
		//trace(this.layout);
		this.towerId = towerId;
	
		this.useHandCursor = true;
		_towerVO = ApplicationMain.instance.get_globalConfig().getTowerById(towerId);
		//var i:Image = new Image();
		
		_normalBack.resource = "img/ui/shop/TowerChose_Back_norm.png";
		_activeBack.resource = "img/ui/shop/TowerChose_Back_active.png";
		addChild(_normalBack);
		addChild(_activeBack);
		_activeBack.visible = false;
		//
		_icon.resource = _towerVO.shopIcon;
		_icon.x = 175;
		_icon.y = 25;
		addChild(_icon);
		//trace(ApplicationMain.instance.get_textManager().getText(I18n.SHOP_TOWER_0));
		//trace(I18n.SHOP_TOWER_1);
		
		_headerText.text = ApplicationMain.instance.get_textManager().getText(_towerVO.name);
		_headerText.y = 20;
		_headerText.x = 65;
		addChild(_headerText);
		
		_headerText.style.color = 0xffffff;
		_headerText.style.fontSize = 20;
		
		
		_amountText.style.color = 0xffffff;
		_amountText.style.fontSize = 12;
		
		_amountText.y = 50;
		_amountText.x = 65;
		addChild(_amountText);
		//trace(ApplicationMain.instance.get_textManager().getText(I18n.SHOP_AMOUNT));
		_amountText.text = Replace.flags(ApplicationMain.instance.get_textManager().getText(I18n.SHOP_AMOUNT),["<value>"],[Std.string(ApplicationMain.instance.get_profileManager().getResourceValue(_towerVO.resourceId))]);
		
		
		
		_priceText.style.color = 0xffffff;
		_priceText.style.fontSize = 16;
		
		_priceText.x = 115;
		_priceText.y = 85;
		addChild(_priceText);
		
		if(isBattle){
			
			_priceText.text = Std.string(_towerVO.price) + " E.P.";
		}else{
			_priceText.text = Std.string(ApplicationMain.instance.get_profileManager().getPrice(_towerVO.resourceId, Resources.RESEARCH_POINTS)) + " R.P.";
		}
		
		addEventListener(MouseEvent.ROLL_OVER, rollOver);
		addEventListener(MouseEvent.ROLL_OUT, rollOut);
	
		ApplicationMain.instance.get_profileManager().addEventListener(ResourceEvent.RESOURCE_CHANGED, resourceChanged);
		
	}
	
	private function resourceChanged(e:ResourceEvent):Void 
	{
		if(e.resId ==_towerVO.resourceId){
			_amountText.text = Replace.flags(ApplicationMain.instance.get_textManager().getText(I18n.SHOP_AMOUNT),["<value>"],[Std.string(e.newCount)]);
		
		
		}
	}
	
	private function rollOut(e:MouseEvent):Void 
	{
		if(!selected){
		_activeBack.visible = false;
		_normalBack.visible = true;
		}
	}
	
	private function rollOver(e:MouseEvent):Void 
	{
		if(!selected){
			_activeBack.visible = true;
			_normalBack.visible = false;
		}
	}
	
	private function enterFrame(e:Event):Void 
	{
		this.layout.refresh();
	}
	
	public function get_selected():Bool 
	{
		return selected;
	}
	
	public function set_selected(value:Bool):Bool 
	{
		if(value){
			_activeBack.visible = true;
			_normalBack.visible = false;
		}
		else{
			_activeBack.visible = false;
			_normalBack.visible = true;
		}
		return selected = value;
	}
	
	override public function dispose():Void 
	{
		//trace("dispose");
		ApplicationMain.instance.get_profileManager().removeEventListener(ResourceEvent.RESOURCE_CHANGED, resourceChanged);
		super.dispose();
	}
}