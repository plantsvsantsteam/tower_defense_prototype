package com.sorochich.gameprototype.shop;

import com.sorochich.gameprototype.common.meta.Resources;
import haxe.ui.toolkit.core.Component;
import openfl.events.Event;
import openfl.events.MouseEvent;
import com.sorochich.gameprototype.shop.ShopRenderer;

/**
 * ...
 * @author s_sorochich
 */
class ShopList extends Component
{
	var _renderer1:ShopRenderer;
	var _renderer2:ShopRenderer;
	var isBattle:Bool;

	public var currentId:String="0";
	public function new(isBattle:Bool = false) 
	{
		super();
		this.isBattle = isBattle;
		this.layout = new ShopRendererLayout();
		//trace(this.layout);
		
		
		_renderer1 = new ShopRenderer("0",isBattle);
		_renderer2 = new ShopRenderer("1",isBattle);
		addChild(_renderer1);
		addChild(_renderer2);
		_renderer2.y = 150;
		
		_renderer1.set_selected(true);
		
		_renderer1.addEventListener(MouseEvent.CLICK, renderer1_click);
		_renderer2.addEventListener(MouseEvent.CLICK, renderer2_click);
	}
	
	private function renderer1_click(e:MouseEvent):Void 
	{
	
		if(!_renderer1.get_selected()){
			_renderer1.set_selected(true);
			_renderer2.set_selected(false);
			currentId = "0";
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
	
	private function renderer2_click(e:MouseEvent):Void 
	{
		if(!_renderer2.get_selected()){
			_renderer2.set_selected(true);
			_renderer1.set_selected(false);
			currentId = "1";
			
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
	
}