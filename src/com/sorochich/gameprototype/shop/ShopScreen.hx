package com.sorochich.gameprototype.shop;

import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.events.ResourceEvent;
import com.sorochich.gameprototype.common.implementations.ScreenBase;
import com.sorochich.gameprototype.common.meta.I18n;
import com.sorochich.gameprototype.common.meta.Resources;
import haxe.ui.toolkit.containers.Box;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.interfaces.IDisplayObject;
import openfl.events.Event;
import openfl.events.IEventDispatcher;
import openfl.events.MouseEvent;
import com.sorochich.gameprototype.common.const.Screen;
/**
 * ...
 * @author s_sorochich
 */
class ShopScreen extends ScreenBase 
{

	private var _shopBox:VBox;
	private var _shopPanelBox:VBox;
	//private var _battleButton:Button;
	private var _view:IDisplayObject;
	private var _root:Root;

	private var _shopList:ShopList = new ShopList();
	private var _shopPanel:ShopPanel = new ShopPanel();
	var _menuButton:Button;
	var _battleButton:Button;
	private var _currencyText: Text;
	
	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	override private function onShow() 
	{
		_shopPanel.horizontalAlign = "center";
		_shopPanel.style.paddingLeft = 320;
		
		_root = ApplicationMain.instance.get_uiRoot();
		
		_view = Toolkit.processXmlResource("layouts/shopLayout.xml");
		_root.addChildAt(_view,0);
		_shopBox = _root.findChild("shopBox", VBox, true);
		_shopPanelBox = _root.findChild("shopPanelBox", VBox, true);
		
		_shopBox.addChildAt(_shopList,0);
		_shopPanelBox.addChildAt(_shopPanel,0);
		
		_shopList.addEventListener(Event.CHANGE, shopList_change);
		_shopPanel.setTowerId("0");

		_menuButton = _root.findChild("menuButton", Button, true);
		_battleButton = _root.findChild("battleButton", Button, true);

		_menuButton.text = ApplicationMain.instance.get_textManager().getText(I18n.MENU_MENU_BUTTON);
		_battleButton.text = ApplicationMain.instance.get_textManager().getText(I18n.MENU_BATTLE_BUTTON);
		
		_battleButton.addEventListener(MouseEvent.CLICK, battleButton_click);
		_menuButton.addEventListener(MouseEvent.CLICK, shopButton_click);
		
		_currencyText = _root.findChild("currencyText", Text, true);
		
		_currencyText.style.color = 0xffffff;
		_currencyText.style.fontSize = 20;
		_currencyText.text = Std.string(ApplicationMain.instance.get_profileManager().getResourceValue(Resources.RESEARCH_POINTS)) + " R.P.";
		ApplicationMain.instance.get_profileManager().addEventListener(ResourceEvent.RESOURCE_CHANGED, resourceChanged);
		
	}
	
	private function shopButton_click(e:MouseEvent):Void 
	{
		ApplicationMain.instance.get_screenManager().showScreen(Screen.MAIN_MENU);
	}
	
	private function battleButton_click(e:MouseEvent):Void 
	{
		ApplicationMain.instance.get_screenManager().showScreen(Screen.PREBATTLE);
	}
	private function shopList_change(e:Event):Void 
	{
		_shopPanel.setTowerId(_shopList.currentId);
		//trace(_shopList.currentId);
	}

	//private function enterFrame(e:Event):Void 
	//{
		//ApplicationMain.instance.get_stage().removeEventListener(Event.ENTER_FRAME, enterFrame);
		//
	//}
	
	private function resourceChanged(e:ResourceEvent):Void 
	{
		if(e.resId == Resources.RESEARCH_POINTS){
			_currencyText.text = Std.string(e.newCount) + " R.P.";
		}
	}
	
	override private function onHide() 
	{
		ApplicationMain.instance.get_profileManager().removeEventListener(ResourceEvent.RESOURCE_CHANGED, resourceChanged);
		_root.removeChild(_view);
		_view.dispose();
		_shopPanel.dispose();
		_shopList.dispose();
		 super.onHide();
	}
	
}