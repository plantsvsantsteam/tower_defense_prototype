package com.sorochich.gameprototype;

import com.sorochich.gameprototype.base.implementations.commands.InitAtlasManagerCommand;
import com.sorochich.gameprototype.base.implementations.commands.InitDataStorageCommand;
import com.sorochich.gameprototype.base.implementations.commands.InitGlobalConfig;
import com.sorochich.gameprototype.base.implementations.commands.InitI18nCommand;
import com.sorochich.gameprototype.base.implementations.commands.InitProfileManagerCommand;
import com.sorochich.gameprototype.base.implementations.commands.InitScreenManagerCommand;
import com.sorochich.gameprototype.base.implementations.commands.InitUIFrameworkCommand;
import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.const.Screen;
import com.sorochich.gameprototype.common.implementations.AtlasVO;
import com.sorochich.gameprototype.common.implementations.CommandsChain;
import com.sorochich.gameprototype.common.interfaces.ICommandsChain;
import com.sorochich.gameprototype.common.meta.I18n;
import com.sorochich.gameprototype.common.meta.Resources;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.core.Toolkit;
import haxe.xml.Fast;
import openfl.Assets;
import openfl.display.Sprite;
import openfl.display.StageAlign;
import openfl.display.StageScaleMode;
import openfl.display.Tilesheet;
import openfl.events.Event;
import openfl.geom.Rectangle;
import openfl.net.SharedObject;
import com.sorochich.gameprototype.profile.implementations.ProfileManager;
import com.sorochich.gameprototype.profile.interfaces.IProfileManager;
import com.sorochich.gameprototype.toppanel.TopPanel;

/**
 * ...
 * @author s_sorochich
 */
 


class Main extends Sprite 
{
	public function new() 
	{
		super();
	
		stage.scaleMode = StageScaleMode.NO_SCALE;
		stage.align = StageAlign.TOP_LEFT;
		ApplicationMain.instance.set_stage(stage);
		
		var initialChain:ICommandsChain = new CommandsChain();
		initialChain.add(new InitI18nCommand());
		initialChain.add(new InitUIFrameworkCommand());
		initialChain.add(new InitAtlasManagerCommand());
		initialChain.add(new InitScreenManagerCommand());
		initialChain.add(new InitDataStorageCommand());
		initialChain.add(new InitProfileManagerCommand());
		initialChain.add(new InitGlobalConfig());
		
		initialChain.addEventListener(Event.COMPLETE, initialChain_complete);
		initialChain.execute();		
		//trace();
					
	
	}
	
	private function initialChain_complete(e:Event):Void 
	{
		e.target.removeEventListener(Event.COMPLETE, initialChain_complete);
		ApplicationMain.instance.get_screenManager().showScreen(Screen.MAIN_MENU);	
		
		
		//new TopPanel();
		testMethod();
		
	}
	
	function testMethod():Void 
	{
		var profile:IProfileManager = ApplicationMain.instance.get_profileManager();
		//profile.contribute(Resources.RESEARCH_POINTS, 300);
		//profile.contribute(Resources.SEED_0, 10);
		
		
		//profile.contribute(Resources.SEED_1, 5);
		
		//profile.buy(Resources.SEED_0, Resources.RESEARCH_POINTS, 10);
		//profile.buy(Resources.SEED_1, Resources.RESEARCH_POINTS, 5);
		
		//trace(profile.getResourceValue(Resources.RESEARCH_POINTS));
		//trace(profile.getResourceValue(Resources.SEED_0));
		//trace(profile.getResourceValue(Resources.SEED_1));
		//so.data.research_points = profile.getResourceValue(Resources.RESEARCH_POINTS);
		//trace();
		
		
		//var testTileSheet:Tilesheet = new Tilesheet(Assets.getBitmapData("img/enemies/enemies_spritesheet.png"));
		////trace(Assets.getText("img/enemies/enemy_0/enemy_0_run.xml"));
		//var xml:Fast = new Fast(Xml.parse(Assets.getText("img/enemies/enemies_spritesheet.xml")).firstElement());
		//
		//for (node in xml.nodes.SubTexture){
			//trace(node.att.name);
			//testTileSheet.addTileRect(new Rectangle(Std.parseInt(node.att.x), Std.parseInt(node.att.y), Std.parseInt(node.att.width), Std.parseInt(node.att.height)));
			//
			//
		//}
		//
		//
		//var container:Sprite = new Sprite();
		//addChild(container);
		//
		//testTileSheet.drawTiles(container.graphics, [100, 100, 0], false);
		
		//var items:Array<AtlasVO> = ApplicationMain.instance.get_atlasManager().getItemsByPrefix("Ant_Run");
		//var ind:Array<Float> = new Array<Float>();
		//var i:Int = 0;
		//for(item in items){
			//ind.push(10 + i * 20);
			//ind.push(100);
			//ind.push(item.index);
			//ind.push(Math.PI);
			//i++;
		//}
		//
		//container.graphics.clear();
		//ApplicationMain.instance.get_atlasManager().draw(container, ind);
	}
	

}
