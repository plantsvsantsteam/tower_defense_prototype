package com.sorochich.gameprototype.common.const;

/**
 * @author s_sorochich
 */
enum Screen 
{
	MAIN_MENU;
	BATTLE;
	SHOP;
	PREBATTLE;
}