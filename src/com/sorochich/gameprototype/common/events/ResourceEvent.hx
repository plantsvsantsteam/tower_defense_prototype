package com.sorochich.gameprototype.common.events;

import openfl.events.Event;

/**
 * ...
 * @author s_sorochich
 */
class ResourceEvent extends Event
{
	public var resId:String;
	public var newCount:Int;
	static public var RESOURCE_CHANGED:String = "resourceChanged";

	public function new(type:String, resId:String, newCount:Int) 
	{
		super(type, false, false);
		this.resId = resId;
		this.newCount = newCount;
		
	}
	
	override public function clone():Event 
	{
		return new ResourceEvent(type, resId, newCount);
	}
	
}