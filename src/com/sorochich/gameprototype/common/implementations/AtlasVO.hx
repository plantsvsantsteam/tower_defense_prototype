package com.sorochich.gameprototype.common.implementations;
import haxe.xml.Fast;
import openfl.geom.Rectangle;

/**
 * ...
 * @author s_sorochich
 */
class AtlasVO
{

	public var rect:Rectangle;
	public var name:String;
	public var index:UInt;
	public function new(xml:Fast, index:UInt) 
	{
		this.index = index;
		rect = new Rectangle(Std.parseInt(xml.att.x), Std.parseInt(xml.att.y), Std.parseInt(xml.att.width), Std.parseInt(xml.att.height));
		name = xml.att.name;
	}
	
}