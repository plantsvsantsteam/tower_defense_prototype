package com.sorochich.gameprototype.common.implementations;

import com.sorochich.gameprototype.common.interfaces.ICommand;
import com.sorochich.gameprototype.common.interfaces.ICommandsChain;
import openfl.errors.Error;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;

/**
 * ...
 * @author s_sorochich
 */
class CommandsChain extends EventDispatcher implements ICommandsChain
{

	private var _commands : Array<ICommand> = new Array<ICommand>();
	private var _chainInProgress: Bool = false;
	
	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	
	/* INTERFACE common.interfaces.ICommandsChain */
	
	public function add(command:ICommand):Void 
	{
		if (command!=null){
			_commands.push(command);
		}else{
			throw new Error("Trying to add null command to chain");
		}
		
	}
	
	public  function execute():Void 
	{
		//_commands.iterator;
		//var c:ICommand;
		//for (c in _commands){
			//c.execute();
		//}
		////c++;
		
		if (_chainInProgress){
			throw new Error("Trying to execute chain that already in progress");
		}else{
			_chainInProgress = true;
			onExecute();
		}
		
	}
	
	private function onExecute() 
	{
		var command:ICommand = _commands.shift();
		if (command!=null){
			command.addEventListener(Event.COMPLETE, command_complete);
			command.execute();
		}else{
			_chainInProgress = false;
			dispatchEvent(new Event(Event.COMPLETE));
		}
	}
	
	private function command_complete(e:Event):Void 
	{
		e.target.removeEventListener(Event.COMPLETE, command_complete);
		onExecute();
		
	}
	
	
}