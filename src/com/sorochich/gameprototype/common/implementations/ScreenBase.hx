package com.sorochich.gameprototype.common.implementations;

import com.sorochich.gameprototype.common.interfaces.IScreen;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;

/**
 * ...
 * @author s_sorochich
 */
class ScreenBase extends EventDispatcher implements IScreen
{

	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	
	/* INTERFACE common.interfaces.IScreen */
	@:final 
	public function show():Void 
	{
		onShow();
	}
	
	private function onShow() 
	{
		
	}
	@:final 
	public function hide():Void 
	{
		onHide();
	}
	
	private function onHide() 
	{
		
	}
	
	
}