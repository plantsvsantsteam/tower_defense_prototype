package com.sorochich.gameprototype.common;
import com.sorochich.gameprototype.common.interfaces.IAtlasManager;
import com.sorochich.gameprototype.common.interfaces.IDataStorage;
import com.sorochich.gameprototype.common.interfaces.IScreenManager;
import com.sorochich.gameprototype.common.interfaces.ITextManager;
import haxe.ui.toolkit.core.Root;
import openfl.display.Stage;
import openfl.errors.Error;
import com.sorochich.gameprototype.profile.interfaces.IProfileManager;
import com.sorochich.gameprototype.config.interfaces.IGlobalConfig;

/**
 * ...
 * @author s_sorochich
 */
class ApplicationMain
{

	public static var instance(get, null):ApplicationMain;
	
	@:isVar
	private var textManager(get, set):ITextManager;
	
	@:isVar
	private var stage(get, set):Stage;
	
	@:isVar
	private var screenManager(get, set):IScreenManager;
	
	@:isVar
	private var profileManager(get, set):IProfileManager;
	
	@:isVar
	private var uiRoot(get, set):Root;
	
	@:isVar
	private var dataStorage(get, set):IDataStorage;	
	
	@:isVar
	private var globalConfig(get, set):IGlobalConfig;
	
	@:isVar
	private var atlasManager(get, set):IAtlasManager;
	
	@:isVar
	private var missionId(get, set):String;
	
    private static function get_instance() {
      if (instance == null) {
        instance = new ApplicationMain();
      }
      return instance;
    }

    private function new () {}  // private constructor
	
	public function get_textManager():ITextManager 
	{
		return textManager;
	}
	
	public function set_textManager(value:ITextManager):ITextManager 
	{
		if (textManager!=null){
			throw new Error("TextManager is already set to application");
		}
		return textManager = value;
	}
	
	public function get_stage():Stage 
	{
		return stage;
	}
	
	public function set_stage(value:Stage):Stage 
	{
		if (stage!=null){
			throw new Error("stage is already set to application");
		}
		return stage = value;
	}
	
	public function get_screenManager():IScreenManager 
	{
		return screenManager;
	}
	
	public function set_screenManager(value:IScreenManager):IScreenManager 
	{
		if (screenManager!=null){
			throw new Error("ScreenManager is already set to application");
		}
		return screenManager = value;
	}
	
	public function get_uiRoot():Root 
	{
		return uiRoot;
	}
	
	public function set_uiRoot(value:Root):Root 
	{
		if (uiRoot != null){
			return uiRoot;
			//throw new Error("uiRoot is already set to application");
		}
		return uiRoot = value;
	}
	
	public function get_profileManager():IProfileManager 
	{
		return profileManager;
	}
	
	public function set_profileManager(value:IProfileManager):IProfileManager 
	{
	
		if (profileManager!=null){
			throw new Error("profileManager is already set to application");
		}
		return profileManager = value;
	}
	
	public function get_dataStorage():IDataStorage 
	{
		return dataStorage;
	}
	
	public function set_dataStorage(value:IDataStorage):IDataStorage 
	{
		if (dataStorage!=null){
			throw new Error("DataStorage is already set to application");
		}
		return dataStorage = value;
	}
	
	public function get_globalConfig():IGlobalConfig 
	{
		return globalConfig;
	}
	
	public function set_globalConfig(value:IGlobalConfig):IGlobalConfig 
	{
		if (globalConfig!=null){
			throw new Error("GlobalConfig is already set to application");
		}
		return globalConfig = value;
	}
	
	public function get_atlasManager():IAtlasManager 
	{
		return atlasManager;
	}
	
	public function set_atlasManager(value:IAtlasManager):IAtlasManager 
	{
		return atlasManager = value;
	}
	
	public function get_missionId():String 
	{
		return missionId;
	}
	
	public function set_missionId(value:String):String 
	{
		return missionId = value;
	}
	

}