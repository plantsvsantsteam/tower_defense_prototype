package com.sorochich.gameprototype.common.interfaces;

import openfl.events.IEventDispatcher;
/**
 * @author s_sorochich
 */
interface ICommand extends IEventDispatcher 
{
	public function execute(): Void;
}