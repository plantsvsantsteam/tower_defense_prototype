package com.sorochich.gameprototype.common.interfaces;

/**
 * @author s_sorochich
 */
interface IDataStorage 
{
	public function load(dataId:String):Dynamic;
	public function save(dataId:String, data:Dynamic):Void;
}