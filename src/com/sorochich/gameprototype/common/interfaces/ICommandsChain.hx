package com.sorochich.gameprototype.common.interfaces;

/**
 * @author s_sorochich
 */
interface ICommandsChain extends ICommand
{
	public function add(command:ICommand):Void;
}