package com.sorochich.gameprototype.common.interfaces;
import openfl.events.IEventDispatcher;

/**
 * @author s_sorochich
 */
interface IScreen extends IEventDispatcher
{
	public function show() : Void;
	public function hide() : Void;
}