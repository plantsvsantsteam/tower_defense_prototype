package com.sorochich.gameprototype.common.interfaces;

/**
 * @author s_sorochich
 */
interface ITextManager 
{
	public function initialize(locale:String, callback:Void->Void = null): Void;
	public function getText(key:String): String;
}