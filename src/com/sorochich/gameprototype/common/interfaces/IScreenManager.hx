package com.sorochich.gameprototype.common.interfaces;
import com.sorochich.gameprototype.common.const.Screen;

/**
 * @author s_sorochich
 */
interface IScreenManager 
{
	public function showScreen(value:Screen): Void;
}