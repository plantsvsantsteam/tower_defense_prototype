package com.sorochich.gameprototype.common.interfaces;
import com.sorochich.gameprototype.common.implementations.AtlasVO;
import openfl.display.DisplayObjectContainer;
import openfl.display.Sprite;
import openfl.events.IEventDispatcher;

/**
 * @author s_sorochich
 */
interface IAtlasManager extends IEventDispatcher
{
	function init(atlasName : String):Void;
	function getItemsByPrefix(prefix:String):Array<AtlasVO>;
	function draw(container:Sprite, data:Array<Float>):Void;
	
	
}