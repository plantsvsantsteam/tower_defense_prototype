package com.sorochich.gameprototype.common.meta;

/**
 * @author s_sorochich
 */
@:build(com.sorochich.gameprototype.macroses.ResourcesGenerator.build())
class Resources
{

	private function new() 
	{
		
	}
	
	static public function getResourceName(id:String):String{
		
		return Resources.ALL_RESOURCES_NAMES[Resources.ALL_RESOURCES.indexOf(id)];
	}
	
}