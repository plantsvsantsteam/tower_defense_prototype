package com.sorochich.gameprototype.mainmenu;

import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.const.Screen;
import com.sorochich.gameprototype.common.events.ResourceEvent;
import com.sorochich.gameprototype.common.implementations.ScreenBase;
import com.sorochich.gameprototype.common.interfaces.IScreen;
import com.sorochich.gameprototype.common.meta.I18n;
import com.sorochich.gameprototype.common.meta.Resources;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.interfaces.IDisplayObject;
import haxe.ui.toolkit.core.interfaces.IDisplayObjectContainer;
import openfl.events.IEventDispatcher;
import openfl.events.MouseEvent;

/**
 * ...
 * @author s_sorochich
 */
class MainMenuScreen extends ScreenBase implements IScreen
{
	private var _shopButton:Button;
	private var _battleButton:Button;
	private var _view:IDisplayObjectContainer;
	private var _root:Root;
	private var _currencyText: Text;
	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		
	}
	
	override private function onShow() 
	{
		_root = ApplicationMain.instance.get_uiRoot();
		
		_view = Toolkit.processXmlResource("layouts/mainMenuLayout.xml");
		_root.addChildAt(_view,0);
		_shopButton = _root.findChild("shopButton", Button, true);
		_battleButton = _root.findChild("battleButton", Button, true);
		//_shopButton.style.fontSize = 20;
		//_shopButton.style.color = 0xffffff;
		
		_shopButton.text = ApplicationMain.instance.get_textManager().getText(I18n.MENU_SHOP_BUTTON);
		_battleButton.text = ApplicationMain.instance.get_textManager().getText(I18n.MENU_BATTLE_BUTTON);
	
		_currencyText = _root.findChild("currencyText", Text, true);
		
		_currencyText.style.color = 0xffffff;
		_currencyText.style.fontSize = 20;
		_currencyText.text = Std.string(ApplicationMain.instance.get_profileManager().getResourceValue(Resources.RESEARCH_POINTS)) + " R.P.";
		ApplicationMain.instance.get_profileManager().addEventListener(ResourceEvent.RESOURCE_CHANGED, resourceChanged);
		//trace(_shopButton.style.fontEmbedded);
		//trace(_shopButton.style.fontName);
		//trace(_shopButton.style.fontScale);
		//trace(_shopButton.style.fontSize);
		//trace(_shopButton.style.color);
		//trace(_shopButton.get__label().visible);
		//
		////trace(.x);
		//trace(_shopButton.get__label().y);
		//trace(_shopButton.get__label().alpha);
		//trace(_shopButton.get__label().alpha);
		//
		//_view.addChild(_shopButton.get__label());
		
		//trace(_shopButton);
		_battleButton.addEventListener(MouseEvent.CLICK, battleButton_click);
		_shopButton.addEventListener(MouseEvent.CLICK, shopButton_click);
		
	}
	
	private function shopButton_click(e:MouseEvent):Void 
	{
		trace(_shopButton.text);
		ApplicationMain.instance.get_screenManager().showScreen(Screen.SHOP);
	}
	
	private function battleButton_click(e:MouseEvent):Void 
	{
		ApplicationMain.instance.get_screenManager().showScreen(Screen.PREBATTLE);
	}


	private function resourceChanged(e:ResourceEvent):Void 
	{
		if(e.resId == Resources.RESEARCH_POINTS){
			_currencyText.text = Std.string(e.newCount) + " R.P.";
		}
	}
	
	override private function onHide() 
	{
		ApplicationMain.instance.get_profileManager().removeEventListener(ResourceEvent.RESOURCE_CHANGED, resourceChanged);
		_root.removeChild(_view);
		_view.dispose();
		_battleButton.removeEventListener(MouseEvent.CLICK, battleButton_click);
		_shopButton.removeEventListener(MouseEvent.CLICK, shopButton_click);
		_battleButton.dispose();
		_shopButton.dispose();
		
		_root = null;
		_battleButton  = null;
		_shopButton = null;
		_view = null;
		
	}
	
}