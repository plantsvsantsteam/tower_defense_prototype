package com.sorochich.gameprototype.battle.entitysystem.enums;

/**
 * @author s_sorochich
 */
enum CellStatus 
{
	EMPTY;
	HAS_TOWER;
	LOCKED;
}