package com.sorochich.gameprototype.battle.entitysystem;
import ash.core.Engine;
import ash.core.Entity;
import ash.fsm.EntityStateMachine;
import com.sorochich.gameprototype.battle.BattleUI;
import com.sorochich.gameprototype.battle.entitysystem.components.Attack;
import com.sorochich.gameprototype.battle.entitysystem.components.AttackGroup;
import com.sorochich.gameprototype.battle.entitysystem.components.Borders;
import com.sorochich.gameprototype.battle.entitysystem.components.Bullet;
import com.sorochich.gameprototype.battle.entitysystem.components.Cell;
import com.sorochich.gameprototype.battle.entitysystem.components.Display;
import com.sorochich.gameprototype.battle.entitysystem.components.Enemy;
import com.sorochich.gameprototype.battle.entitysystem.components.Growth;
import com.sorochich.gameprototype.battle.entitysystem.components.Gun;
import com.sorochich.gameprototype.battle.entitysystem.components.Health;
import com.sorochich.gameprototype.battle.entitysystem.components.Mission;
import com.sorochich.gameprototype.battle.entitysystem.components.PathRunner;
import com.sorochich.gameprototype.battle.entitysystem.components.Position;
import com.sorochich.gameprototype.battle.entitysystem.components.Reward;
import com.sorochich.gameprototype.battle.entitysystem.components.Speed;
import com.sorochich.gameprototype.battle.entitysystem.components.Target;
import com.sorochich.gameprototype.battle.entitysystem.components.TilesheetAnimation;
import com.sorochich.gameprototype.battle.entitysystem.components.Tower;
import com.sorochich.gameprototype.battle.entitysystem.components.UI;
import com.sorochich.gameprototype.battle.entitysystem.components.Wave;
import com.sorochich.gameprototype.battle.entitysystem.components.WaveMarker;
import com.sorochich.gameprototype.battle.entitysystem.nodes.CellNode;
import com.sorochich.gameprototype.battle.entitysystem.nodes.EnemyNode;
import com.sorochich.gameprototype.battle.entitysystem.states.EnemyStates;
import com.sorochich.gameprototype.battle.entitysystem.states.TowerStates;
import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.implementations.AtlasVO;
import com.sorochich.gameprototype.common.interfaces.IAtlasManager;
import com.sorochich.gameprototype.config.vo.AttackVO;
import com.sorochich.gameprototype.config.vo.BulletVO;
import com.sorochich.gameprototype.config.vo.CellSchemeVO;
import com.sorochich.gameprototype.config.vo.MissionVO;
import com.sorochich.gameprototype.config.vo.TowerVO;
import com.sorochich.gameprototype.config.vo.WaveVO;
import flash.display.Bitmap;
import openfl.Assets;
import openfl.display.PixelSnapping;
import openfl.display.Sprite;
import openfl.geom.Point;
import com.sorochich.gameprototype.profile.interfaces.IProfileManager;
import com.sorochich.gameprototype.config.interfaces.IGlobalConfig;
import com.sorochich.gameprototype.config.vo.EnemyVO;

/**
 * ...
 * @author s_sorochich
 */
class EntityCreator
{
	private var _engine:Engine;
	private var _globalConfig:IGlobalConfig;
	private var _profile:IProfileManager;
	private var _atlas:IAtlasManager;
	

	public function new(engine:Engine) 
	{
		_globalConfig = ApplicationMain.instance.get_globalConfig();
		_engine = engine;
		_profile = ApplicationMain.instance.get_profileManager();
		_atlas = ApplicationMain.instance.get_atlasManager();
		
	}
	
	public function createTestEntity():Entity{
		
		var testShape: Sprite = new Sprite();
		testShape.graphics.beginFill(0xff);
		testShape.graphics.drawRect(0, 0, 100, 100);
		testShape.graphics.endFill();
		var bullet : Entity = new Entity()
				.add( new Position(400*Math.random(),400*Math.random(), 360*Math.random()) )

				.add( new Display( testShape ) );
		_engine.addEntity( bullet );
		
		return bullet;
	}
	
	public function createEnemy(id:String, pathId:String, wave:Wave):Entity{
		
		var config:EnemyVO = _globalConfig.getEnemyById(id);
		//var bitmap:Bitmap = new Bitmap(Assets.getBitmapData(config.imgRun),PixelSnapping.AUTO, true);
		
		var enemy : Entity = new Entity();
		var animation:Array<AtlasVO> = _atlas.getItemsByPrefix(config.imgRun);
		var animation2:Array<AtlasVO> = _atlas.getItemsByPrefix(config.imgDead);
		var stateMachine:EntityStateMachine = new EntityStateMachine(enemy);
			
		stateMachine.createState(EnemyStates.ALIVE)
								.add(TilesheetAnimation).withInstance(new TilesheetAnimation(animation,config.updateFrame,Std.random(animation.length)))
								.add(PathRunner).withInstance(new PathRunner(pathId))
								.add(Reward).withInstance(new Reward(config.reward))
								.add(Speed).withInstance(new Speed(config.speed))
								.add(Health).withInstance(new  Health(config.health));
		stateMachine.createState(EnemyStates.DEAD)
								.add(TilesheetAnimation).withInstance(new TilesheetAnimation(animation2, 10, 0, false));		
		
		var e:Enemy = new Enemy(stateMachine);
		var animation:Array<AtlasVO> = _atlas.getItemsByPrefix(config.imgRun);
		
				enemy.add( new Position(0,0, 0 ))
				.add(wave)
				.add(new Borders(_atlas.getItemsByPrefix(config.imgRun)[0].rect))
				.add(e);
				
		
		e.set_state(EnemyStates.ALIVE);
		_engine.addEntity( enemy );
		
		return enemy;
	}
	
	public function createWave(waveId:String):Entity{
		
		var waveInfo:WaveVO = _globalConfig.getWaveById(waveId);
		var wave : Entity = new Entity().add(new Wave(waveInfo.count, waveInfo.interval, waveInfo.pathId, waveInfo.enemyId))
										.add(new WaveMarker());

		_engine.addEntity(wave);
		return wave;
	}
	public function destroyEntity( entity : Entity ) : Void
	{
		_engine.removeEntity( entity );
	}
	
	public function createAttack(attackVO:AttackVO):Entity 
	{
		var groups:Array<AttackGroup> = new Array<AttackGroup>();
		for (g in attackVO.groups){
			groups.push(new AttackGroup(g.delay, g.waveId));
		}
		var attack:Attack = new Attack();
		attack.groups = groups;
		
		var e:Entity = new Entity().add(attack);
		_engine.addEntity(e);
		return e;
	}
	
	public function createMission(id:String) 
	{
		var missionInfo:MissionVO = _globalConfig.getMissionById(id);
		var bitmap:Bitmap = new Bitmap(Assets.getBitmapData(missionInfo.img),PixelSnapping.AUTO,true);
		
		
		var mission : Entity = new Entity()
				.add(new Position(0,0, 0 ))
				.add(new Display( bitmap ))
				.add(new Mission(missionInfo))
				.add(new UI(new BattleUI()))
				.add(new Health(missionInfo.baseHealth));
				
				
		_engine.addEntity( mission );
		
		
		var cellSchema:CellSchemeVO = _globalConfig.getCellSchemeById(missionInfo.cellId);
		
		
		var cellBitmapData = Assets.getBitmapData("img/maps/cell.png");
		
		for (cellVO in cellSchema.cells){
			//var cellBitmap
			var cellSprite:Sprite = new Sprite();
			cellSprite.addChild(new Bitmap(cellBitmapData));
			var cell:Entity = new Entity()
				.add(new Position(cellVO.x, cellVO.y, 0))
				.add(new Display(cellSprite))
				.add(new Cell());
				
			_engine.addEntity(cell);
			
			
		}
		
		
		return mission;
		
		
	}
	
	public function createTower(cell:CellNode, id:String, mission : Mission):Entity 
	{
		var towerVO:TowerVO = _globalConfig.getTowerById(id);
		if(_profile.getResourceValue(towerVO.resourceId)>0 && mission.moneyValue>= towerVO.price){
			_profile.contribute(towerVO.resourceId, -1);
			mission.moneyValue-= towerVO.price;
			
			var tower:Entity = new Entity();
			var stateMachine:EntityStateMachine = new EntityStateMachine(tower);
			
			stateMachine.createState(TowerStates.GROWTH_0)
								.add(Display).withInstance(new Display(new Bitmap(Assets.getBitmapData(towerVO.assets.growth.growth_0),PixelSnapping.AUTO,true)));
			
			stateMachine.createState(TowerStates.GROWTH_1)
								.add(Display).withInstance(new Display(new Bitmap(Assets.getBitmapData(towerVO.assets.growth.growth_1),PixelSnapping.AUTO,true)));	
								
			stateMachine.createState(TowerStates.GROWTH_2)
								.add(Display).withInstance(new Display(new Bitmap(Assets.getBitmapData(towerVO.assets.growth.growth_2),PixelSnapping.AUTO,true)));	
			var s:Sprite = new Sprite();
			s.addChild(new Bitmap(Assets.getBitmapData(towerVO.assets.active),PixelSnapping.AUTO,true));
			
			stateMachine.createState(TowerStates.ALIVE)
								.add(Display).withInstance(new Display(s))
								.add(Gun).withInstance(new Gun(towerVO.gun.reloadingTime, towerVO.gun.distance, towerVO.gun.bulletId));
			
			tower.add(cell.position)
				.add(new Growth(towerVO.growthTime, towerVO.assets.growth, towerVO))
				.add( new Tower(stateMachine));
				
			stateMachine.changeState(TowerStates.GROWTH_0);
			_engine.addEntity(tower);
			return tower;
			
		}
		
		return null;
		
	}
	
	public function createBullet(bulletId:String, target:EnemyNode, startPosition:Point):Entity  
	{
		var config:BulletVO = _globalConfig.getBulletById(bulletId);
		
		var bullet:Entity = new Entity();
		bullet.add(new Display(new Bitmap(Assets.getBitmapData(config.img)), true));
		bullet.add(new Position(startPosition.x, startPosition.y, 0));
		bullet.add(new Target(target.position,target.borders.rect));
		bullet.add(target.entity.get(Health));
		bullet.add(new Bullet(config.damage));
		bullet.add(new Speed(config.speed));
		
		_engine.addEntity(bullet);
		return bullet;
	}
	
}