package com.sorochich.gameprototype.battle.entitysystem.systems;

import ash.core.Entity;
import ash.tools.ListIteratingSystem;
import com.sorochich.gameprototype.battle.entitysystem.EntityCreator;
import com.sorochich.gameprototype.battle.entitysystem.components.Attack;
import com.sorochich.gameprototype.battle.entitysystem.components.AttackGroup;
import com.sorochich.gameprototype.battle.entitysystem.components.Wave;
import com.sorochich.gameprototype.battle.entitysystem.nodes.AttackNode;

/**
 * ...
 * @author s_sorochich
 */
class AttackSystem extends ListIteratingSystem<AttackNode>
{
	private var _creator:EntityCreator;

	public function new(creator:EntityCreator) 
	{
		super(AttackNode, nodeUpdate, nodeAdded);
		_creator = creator;
	}
	
	private function nodeAdded(node:AttackNode) 
	{
		
	}
	
	private function nodeUpdate(node:AttackNode, time:Float) 
	{
		var completeNum:Int = 0;
		var groups:Array<AttackGroup> = node.attack.groups;
		for (g in groups){
			if (!g.started && (g.timeBeforeStart -= time) <= 0)
			{
				g.started = true;
				g.wave = _creator.createWave(g.waveId).get(Wave);
			}else
			{
				if(g.started && g.wave.complete)
				{
					
					completeNum++;	
				}
			}
			
		}
		
		if (completeNum == groups.length){
			node.attack.complete = true;
			_creator.destroyEntity(node.entity);
		}
	}
	
}