package com.sorochich.gameprototype.battle.entitysystem.systems;

import ash.tools.ListIteratingSystem;
import com.sorochich.gameprototype.battle.entitysystem.components.PathRunner;
import com.sorochich.gameprototype.battle.entitysystem.components.Position;
import com.sorochich.gameprototype.battle.entitysystem.components.Speed;
import com.sorochich.gameprototype.battle.entitysystem.nodes.PathRunnerNode;
import com.sorochich.gameprototype.common.ApplicationMain;
import openfl.geom.Point;
import com.sorochich.gameprototype.config.interfaces.IGlobalConfig;

/**
 * ...
 * @author s_sorochich
 */
class PathRunningSystem extends ListIteratingSystem<PathRunnerNode>
{
	private var _globalConfig:IGlobalConfig;

	public function new() 
	{
		_globalConfig = ApplicationMain.instance.get_globalConfig();
		super(PathRunnerNode, updateFunction, nodeAdded);
		
	}
	
	private function nodeAdded(node:PathRunnerNode) 
	{
		changePathPoint(node,0);
		
	}
	
	private function updateFunction( node : PathRunnerNode, time : Float ) : Void
	{
		
		var pathRunner:PathRunner = node.pathRunner;
		var position:Position = node.position;
		var speed:Speed = node.speed;
		
		pathRunner.passedPath += time * speed.value;

		while (pathRunner.passedPath> pathRunner.currentSegmentLength && !pathRunner.pathComplete){
			changePathPoint(node, pathRunner.currentPointNum + 1);
		}
		position.position.x =pathRunner.currentPoint.x + pathRunner.currentSegment.x * pathRunner.passedPath / pathRunner.currentSegmentLength;
		position.position.y =pathRunner.currentPoint.y + pathRunner.currentSegment.y * pathRunner.passedPath / pathRunner.currentSegmentLength;
		
		position.rotation = Math.atan2(pathRunner.currentSegment.y, pathRunner.currentSegment.x)/Math.PI*180;
		
	}
	
	function changePathPoint(node:PathRunnerNode,pointNum:UInt):Void 
	{
		var pathRunner:PathRunner = node.pathRunner;
		var pathId:String = pathRunner.pathId;
		var firstPoint: Point = _globalConfig.getPathPoint(pathId, pointNum);
		var secondPoint: Point = _globalConfig.getPathPoint(pathId, pointNum+1);
		if (firstPoint == null || secondPoint == null){
			//trace(pathRunner.pathComplete );
			pathRunner.pathComplete = true;
		}else{
		
			var x:Float = firstPoint.x;
			var y:Float = firstPoint.y;
			
			
			var position:Position = node.position;
			
			position.position.x = x;
			position.position.y = y;
			
			pathRunner.currentPoint = firstPoint;
			pathRunner.currentSegment = secondPoint.subtract(firstPoint);
			
			

			if(pointNum==0){
				pathRunner.passedPath = 0;
			}else{
				pathRunner.passedPath -= pathRunner.currentSegmentLength;
			}
			pathRunner.currentSegmentLength = pathRunner.currentSegment.length;
			pathRunner.currentPointNum = pointNum;
			
		}
	}
	
	
}