package com.sorochich.gameprototype.battle.entitysystem.systems;

import ash.fsm.EntityStateMachine;
import ash.tools.ListIteratingSystem;
import com.sorochich.gameprototype.battle.entitysystem.components.Growth;
import com.sorochich.gameprototype.battle.entitysystem.components.Tower;
import com.sorochich.gameprototype.battle.entitysystem.nodes.GrowthNode;
import com.sorochich.gameprototype.battle.entitysystem.states.TowerStates;
import flash.display.Bitmap;
import openfl.Assets;
import openfl.display.Sprite;


/**
 * ...
 * @author s_sorochich
 */
class GrowthSystem extends ListIteratingSystem<GrowthNode>
{

	public function new() 
	{
		super(GrowthNode, nodeUpate);
		
	}
	
	private function nodeUpate(node:GrowthNode, time:Float):Void{
		
		var growth:Growth = node.growth;
		var towerStates:EntityStateMachine = node.tower.stateMachine;

		
		var prevTime:Float = growth.timeLeft/growth.growthTime;
		growth.timeLeft -= time;
		
		var currentTime:Float = growth.timeLeft/growth.growthTime;
		if(prevTime>0.66 && currentTime<=0.66){
			node.tower.stateMachine.changeState(TowerStates.GROWTH_1);
		}else{
			if(prevTime>0.33 && currentTime<=0.33){
				node.tower.stateMachine.changeState(TowerStates.GROWTH_2);

			}else{
				if(prevTime>0 && currentTime<=0){
					node.tower.stateMachine.changeState(TowerStates.ALIVE);
					node.entity.remove(Growth);
				}
				
			}
		}
	}
	
	
	
}