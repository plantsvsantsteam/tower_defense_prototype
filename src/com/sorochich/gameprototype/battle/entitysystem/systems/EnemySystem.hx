package com.sorochich.gameprototype.battle.entitysystem.systems;

import ash.core.Engine;
import ash.core.Entity;
import ash.core.NodeList;
import ash.tools.ListIteratingSystem;
import com.sorochich.gameprototype.battle.entitysystem.EntityCreator;
import com.sorochich.gameprototype.battle.entitysystem.components.Health;
import com.sorochich.gameprototype.battle.entitysystem.components.Mission;
import com.sorochich.gameprototype.battle.entitysystem.components.PathRunner;
import com.sorochich.gameprototype.battle.entitysystem.components.Reward;
import com.sorochich.gameprototype.battle.entitysystem.nodes.EnemyNode;
import com.sorochich.gameprototype.battle.entitysystem.nodes.MissionNode;
import com.sorochich.gameprototype.battle.entitysystem.states.EnemyStates;

/**
 * ...
 * @author s_sorochich
 */
class EnemySystem extends ListIteratingSystem<EnemyNode>
{
	private var _creator:EntityCreator;
	private var _missionNode:NodeList<MissionNode>;

	public function new(creator:EntityCreator) 
	{
		super(EnemyNode, updateFunction);
		_creator = creator;
		
	}
	
	override public function addToEngine(engine:Engine):Void 
	{
		_missionNode = engine.getNodeList(MissionNode);
		super.addToEngine(engine);
	}
	
	private function updateFunction( node : EnemyNode, time : Float ) : Void
	{
		var destroyEntity = false;
		//trace(node.pathRunner.pathComplete);
		
		var enemyEntity:Entity = node.entity;
		if (node.enemy.get_state() == EnemyStates.ALIVE){
			if (enemyEntity.get(PathRunner).pathComplete){
				_missionNode.head.health.current--;		
				destroyEntity = true;					
			}
			if(enemyEntity.get(Health).current<=0){
				_missionNode.head.mission.moneyValue+= enemyEntity.get(Reward).value;
				node.enemy.set_state(EnemyStates.DEAD);
			}
			
			
			
		}
		
		if(node.enemy.get_state()==EnemyStates.DEAD&& node.animation.complete){
			destroyEntity = true;
		}
		if(destroyEntity){
			node.wave.enemiesLeftInWave--;
			_creator.destroyEntity(node.entity);	
		}
		
		
	}
	
}