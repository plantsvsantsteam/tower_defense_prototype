package com.sorochich.gameprototype.battle.entitysystem.systems;

import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;
import ash.tick.FrameTickProvider;
import com.sorochich.gameprototype.battle.BattleUI;
import com.sorochich.gameprototype.battle.entitysystem.EntityCreator;
import com.sorochich.gameprototype.battle.entitysystem.components.Attack;
import com.sorochich.gameprototype.battle.entitysystem.components.Mission;
import com.sorochich.gameprototype.battle.entitysystem.nodes.CellNode;
import com.sorochich.gameprototype.battle.entitysystem.nodes.MissionNode;
import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.meta.Resources;
import openfl.display.DisplayObject;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import com.sorochich.gameprototype.common.const.Screen;

/**
 * ...
 * @author s_sorochich
 */
class MissionSystem extends System
{

	public var battleUI:BattleUI;
	public var mission:Mission;
	public var node:MissionNode;
	private var nodes : NodeList<MissionNode>;
	private var _cells : NodeList<CellNode>;
	private var _creator : EntityCreator;
	var ticker:FrameTickProvider;
	var _curentCell: CellNode;
	public var container:Sprite;
	

	public function new(creator : EntityCreator, container:Sprite, ticker:FrameTickProvider) 
	{
		super();
		this.ticker = ticker;
		this.container = container;
		_creator = creator;
		
	}
	
	
	override public function addToEngine( engine : Engine ) : Void
		{
			nodes = engine.getNodeList( MissionNode );
			_cells = engine.getNodeList( CellNode );

			container.addEventListener(MouseEvent.CLICK, container_click);
			nodes.nodeAdded.add( addToDisplay );
			_cells.nodeAdded.add( onCellAdded );
			//nodes.nodeRemoved.add( removeFromDisplay );
		}
		
		private function container_click(e:MouseEvent):Void 
		{
			if(ticker.playing){
				ticker.stop();
				
				battleUI.buttonsBox.visible = true;
				for (cell in _cells){
					if(cell.display.displayObject == e.target ){
						trace("click");
						//_creator.createTower(cell, "0");
						_curentCell = cell;
						battleUI.shopBox.visible = true;
						battleUI.plantButton.addEventListener(MouseEvent.CLICK, plantButton_click);
						break;
					}
				}
				
				battleUI.styledBox.visible = true;
				battleUI.plantButton.visible = _curentCell != null;
				battleUI.styledBox.addEventListener(MouseEvent.CLICK, styledBox_click);
			}else{
				resume();
			}
		}
		
		private function plantButton_click(e:MouseEvent):Void 
		{
			_creator.createTower(_curentCell, battleUI.shopList.currentId, mission);
			resume();
			
		}
		
		private function styledBox_click(e:MouseEvent):Void 
		{
			resume();
		}
		
		private function onCellAdded(node:CellNode) 
		{
			//node.display.displayObject.addEventListener(MouseEvent.CLICK, onCellClick);
		}
		
		
		private function addToDisplay( node : MissionNode ) : Void
		{
			mission = node.mission; 
			battleUI = node.ui.ui; 
			this.node = node; 
			
			hideUI();
			
			
			battleUI.hpText.text = Std.string(node.health.current) + " HP";
			battleUI.moneyText.text = Std.string(mission.moneyValue) + " E.P.";
			mission.currentAttack = _creator.createAttack(mission.missionInfo.attacks[mission.currentAttackNum]).get(Attack);
		}
		
		function hideUI():Void 
		{
			battleUI.buttonsBox.visible = false;
			battleUI.shopBox.visible = false;
			battleUI.styledBox.visible = false;
		}
		
		function resume():Void 
		{
			_curentCell = null;
			hideUI();
			ticker.start();
		}

		
		override public function update( time : Float ) : Void
		{
			
			if (mission != null){
				
				battleUI.hpText.text = Std.string(node.health.current) + " HP";
				battleUI.moneyText.text = Std.string(mission.moneyValue) + " E.P.";
				if (node.health.current == 0){
					trace("health");
					ApplicationMain.instance.get_screenManager().showScreen(Screen.MAIN_MENU);			
					return;
				}
				if(mission.currentAttack.complete){
					mission.currentAttackNum++;
					if(mission.currentAttackNum<mission.attacksNum){
						
						mission.currentAttack = _creator.createAttack(mission.missionInfo.attacks[mission.currentAttackNum]).get(Attack);
					}
					else{
						trace("MissionComplete");
						//_creator.destroyEntity(node.entity);
						
						var prevStatus:String = ApplicationMain.instance.get_profileManager().getMissionStatus(mission.missionInfo.id);
						
						if(prevStatus== null){
							prevStatus = "0";
						}
						var status = "0";
						if(node.health.current ==node.health.total){
							status = "3";
						}else{
							if(node.health.current/node.health.total>0.66){
								status = "2";
							}
							else{
								if(node.health.current/node.health.total>0.33){
									status = "1";
								}
							}
						}
						if(Std.parseInt(status)>Std.parseInt(prevStatus)){
							ApplicationMain.instance.get_profileManager().saveMissionStatus(mission.missionInfo.id, status);
							ApplicationMain.instance.get_profileManager().contribute(Resources.RESEARCH_POINTS, 25 * (Std.parseInt(status) - Std.parseInt(prevStatus) + 1)); 
						}

						ApplicationMain.instance.get_screenManager().showScreen(Screen.PREBATTLE);				
						
					}
					
				}
			}
		}
		
		override public function removeFromEngine(engine:Engine):Void 
		{
			//trace("Remove frome engine");
			battleUI.dispose();
			nodes = null;
			_cells = null;
			node = null;
			_creator = null;
			super.removeFromEngine(engine);
		}
	
}