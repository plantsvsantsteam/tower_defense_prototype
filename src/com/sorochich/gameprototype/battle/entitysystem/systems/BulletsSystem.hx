package com.sorochich.gameprototype.battle.entitysystem.systems;

import ash.tools.ListIteratingSystem;
import com.sorochich.gameprototype.battle.entitysystem.EntityCreator;
import com.sorochich.gameprototype.battle.entitysystem.nodes.BulletNode;
import openfl.geom.Point;
import openfl.geom.Rectangle;


/**
 * ...
 * @author s_sorochich
 */
class BulletsSystem extends ListIteratingSystem<BulletNode>
{
	private var _creator:EntityCreator;

	public function new(creator:EntityCreator) 
	{
		super(BulletNode, updateNode);
		_creator = creator;
		
	}
	
	private function updateNode(node:BulletNode, time:Float):Void
    {
		var targetPostion:Point = node.target.position.position;
		var targetRotation:Float = (node.target.position.rotation)/180*Math.PI;
		var targetRect:Rectangle = node.target.rect;
		var bulletPosition: Point = node.position.position; 
		var targetX:Float = targetPostion.x-targetRect.height/2*Math.cos(targetRotation) - bulletPosition.x;
        var targetY:Float = targetPostion.y-targetRect.width/2*Math.sin(targetRotation)  - bulletPosition.y;
		node.position.rotation = Math.atan2(targetY, targetX) * 180 / Math.PI;

        var vx:Float = node.speed.value*time * (90 - Math.abs(node.position.rotation )) / 90;
        var vy:Float;
           if (node.position.rotation < 0){
               vy = -node.speed.value*time + Math.abs(vx);
		   }
           else{
               vy = node.speed.value*time - Math.abs(vx);
		   }

          node.position.position.x += vx;
          node.position.position.y += vy;
		  
		  if (distance(node.position.position, new Point(targetPostion.x-targetRect.height/2*Math.cos(targetRotation), targetPostion.y-targetRect.width/2*Math.sin(targetRotation))) < 5){
			  //trace("Destroy entity");
			  _creator.destroyEntity(node.entity);
			  node.targetHealth.current -= node.bullet.damage;
		  }
          // process the node here
    }
	
	private function distance(p1:Point, p2:Point):Float{
		return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
	}
	
	
}