package com.sorochich.gameprototype.battle.entitysystem.systems;

import ash.tools.ListIteratingSystem;
import com.sorochich.gameprototype.battle.entitysystem.EntityCreator;
import com.sorochich.gameprototype.battle.entitysystem.components.Wave;
import com.sorochich.gameprototype.battle.entitysystem.nodes.WaveNode;

/**
 * ...
 * @author s_sorochich
 */
class WaveSystem extends ListIteratingSystem<WaveNode>
{

	private var _creator:EntityCreator;
	public function new(creator:EntityCreator) 
	{
		_creator = creator;
		super(WaveNode, updateFunction);
		
	}
	private function updateFunction( node : WaveNode, time : Float ) : Void
	{
		//trace(time);
		var wave:Wave = node.wave;
		//trace(wave.currentTimeBeforeNewEnemy);
		if(!wave.complete && !(wave.enemiesLeftToCreate == 0)  && (wave.currentTimeBeforeNewEnemy-=time)<=0){
			
			if (wave.enemiesLeftToCreate > 0){
				_creator.createEnemy(wave.enemyId,wave.pathId, wave);
				wave.currentTimeBeforeNewEnemy = wave.timeBeforeNewEnemy;
				wave.enemiesLeftToCreate--;
			}
			
				
			
		}
		if (wave.enemiesLeftInWave == 0){
					_creator.destroyEntity(node.entity);
					wave.complete = true;
		}
	}
	
}