package com.sorochich.gameprototype.battle.entitysystem.systems;

import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;
import com.sorochich.gameprototype.battle.entitysystem.components.Display;
import com.sorochich.gameprototype.battle.entitysystem.components.Position;
import com.sorochich.gameprototype.battle.entitysystem.components.TilesheetAnimation;
import com.sorochich.gameprototype.battle.entitysystem.nodes.RenderNode;
import com.sorochich.gameprototype.battle.entitysystem.nodes.TilesheetNode;
import com.sorochich.gameprototype.common.ApplicationMain;
import openfl.display.DisplayObject;
import openfl.display.DisplayObjectContainer;
import openfl.display.Sprite;

/**
 * ...
 * @author s_sorochich
 */
class RenderSystem extends System
{
	public var container : Sprite;
		
	private var nodes : NodeList<RenderNode>;
	private var tileNodes : NodeList<TilesheetNode>;
	private var spritesheetContainer:Sprite;
	

	public function new(container:Sprite,spritesheetContainer) 
	{
		super();
		this.spritesheetContainer = spritesheetContainer;
		
		this.container = container;
		
	}
	
	override public function addToEngine( engine : Engine ) : Void
		{
			nodes = engine.getNodeList( RenderNode );
			tileNodes = engine.getNodeList( TilesheetNode );
			for (node in nodes)
				addToDisplay( node );

			nodes.nodeAdded.add( addToDisplay );
			nodes.nodeRemoved.add( removeFromDisplay );
		}
		
		private function addToDisplay( node : RenderNode ) : Void
		{
			container.addChild( node.display.displayObject );
			addPosition(node);
		}
		
		private function removeFromDisplay( node : RenderNode ) : Void
		{
			container.removeChild( node.display.displayObject );
		}
		
		function addPosition(node:RenderNode):Void 
		{
			var position : Position;
			var display : Display;
			var displayObject : DisplayObject;
			display = node.display;
			displayObject = display.displayObject;
			position = node.position;
			
		
			if (display.centrate){
				displayObject.x = position.position.x-displayObject.width/2*Math.cos(position.rotation/180*Math.PI);
				displayObject.y = position.position.y-displayObject.height/2*Math.sin(position.rotation/180*Math.PI);
				
			}else{
				displayObject.x = position.position.x;
				displayObject.y = position.position.y;
			}
			displayObject.rotation = position.rotation;
		}
		
		override public function update( time : Float ) : Void
		{
			var node : RenderNode;
			var position : Position;
			var display : Display;
			var displayObject : DisplayObject;
			
			for (node in nodes)
			{
				addPosition(node);
			}
			
			var tileNode:TilesheetNode;
			var animation:TilesheetAnimation;
			
			var data:Array<Float> = new Array<Float>();
			for (tileNode in tileNodes){
				
				position = tileNode.position;
				animation = tileNode.animation;
				if (animation.framesBeforeUpdate == 0){
					animation.framesBeforeUpdate = animation.framesBeforeUpdateTotal;
					animation.currentFrame++;
				}
				if (animation.currentFrame == animation.totalFrames){
					if(animation.repeatable){
						animation.currentFrame = 0;
					}else{
						animation.complete = true;
					}
				}
				
			if(!animation.complete){
				data.push(position.position.x-animation.animation[animation.currentFrame].rect.height/2*Math.cos((position.rotation+90)/180*Math.PI));
				data.push(position.position.y-animation.animation[animation.currentFrame].rect.width/2*Math.sin((position.rotation+90)/180*Math.PI));
				data.push(animation.animation[animation.currentFrame].index);
				data.push((position.rotation + 90) / 180 * Math.PI);
				
				
				animation.framesBeforeUpdate--;
			}
			}
			spritesheetContainer.graphics.clear();
			ApplicationMain.instance.get_atlasManager().draw( spritesheetContainer, data);
			
		}
}