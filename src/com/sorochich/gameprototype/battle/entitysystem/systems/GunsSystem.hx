package com.sorochich.gameprototype.battle.entitysystem.systems;

import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;
import com.sorochich.gameprototype.battle.entitysystem.EntityCreator;
import com.sorochich.gameprototype.battle.entitysystem.components.Gun;
import com.sorochich.gameprototype.battle.entitysystem.components.Health;
import com.sorochich.gameprototype.battle.entitysystem.components.PathRunner;
import com.sorochich.gameprototype.battle.entitysystem.nodes.EnemyNode;
import com.sorochich.gameprototype.battle.entitysystem.nodes.GunNode;
import com.sorochich.gameprototype.battle.entitysystem.states.EnemyStates;
import haxe.ds.HashMap;
import openfl.geom.Point;

/**
 * ...
 * @author s_sorochich
 */
class GunsSystem extends System
{

	private var _gunNodes:NodeList<GunNode>;
	private var _enemiesNodes:NodeList<EnemyNode>;
	
	private var _gunToTarget: Map<GunNode, EnemyNode> = new Map<GunNode, EnemyNode>();
	private var _creator:EntityCreator;
	public function new(creator:EntityCreator) 
	{
		super();
		_creator = creator;
		
	}
	
	override public function addToEngine(engine:Engine):Void 
	{
		_gunNodes = engine.getNodeList(GunNode);
		_enemiesNodes = engine.getNodeList(EnemyNode);
	}

	
	override public function update(time:Float):Void 
	{
		for(gunNode in _gunNodes){
			var gun:Gun = gunNode.gun;
			if(gun.timeBeforeShot >0){
				gun.timeBeforeShot -= time;
				if(gun.timeBeforeShot <0){
					gun.timeBeforeShot = 0;
				}
			}
			
			if (!gun.hasTarget){
				for (enemyNode in _enemiesNodes){
					if (distance(gunNode.position.position.add(new Point(64,64)), enemyNode.position.position) <= gun.distance && enemyNode.enemy.get_state() == EnemyStates.ALIVE){
						//trace("target founded");
						_gunToTarget.set(gunNode, enemyNode);
						gun.hasTarget = true;
						break;
					}
				}
				
			}
			
			if(gun.hasTarget){
				var target:EnemyNode = _gunToTarget.get(gunNode);
				
				if (target.entity.get(PathRunner).pathComplete || target.enemy.get_state() == EnemyStates.DEAD || (distance(gunNode.position.position.add(new Point(64,64)), target.position.position) > gun.distance || target.entity.get(Health).current<=0)){
					//trace("target lost");
					gun.hasTarget = false;
					_gunToTarget.remove(gunNode);
				}
				else{
					if (gun.timeBeforeShot == 0){
						//trace("Make shot");
						gun.timeBeforeShot = gun.totalReloadTime;
						_creator.createBullet(gun.bulletId, target, gunNode.position.position.add(new Point(64,64)));
					}
				}
			}
		}
		
	}
	
	
	private function distance(p1:Point, p2:Point):Float{
		return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
	}
	
	override public function removeFromEngine(engine:Engine):Void 
	{
		//trace("Remove From engine");
		_gunNodes = null;
	}
}