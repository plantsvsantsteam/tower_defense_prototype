package com.sorochich.gameprototype.battle.entitysystem.nodes;

import ash.core.Node;
import com.sorochich.gameprototype.battle.entitysystem.components.Cell;
import com.sorochich.gameprototype.battle.entitysystem.components.Display;
import com.sorochich.gameprototype.battle.entitysystem.components.Position;

/**
 * ...
 * @author s_sorochich
 */
class CellNode extends Node<CellNode>
{
	
	public var position:Position;
	public var display:Display;
	public var cell:Cell;

	public function new() 
	{
		
	}
	
}