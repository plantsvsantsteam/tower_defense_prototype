package com.sorochich.gameprototype.battle.entitysystem.nodes;

import ash.core.Node;
import com.sorochich.gameprototype.battle.entitysystem.components.Gun;
import com.sorochich.gameprototype.battle.entitysystem.components.Position;

/**
 * ...
 * @author s_sorochich
 */
class GunNode extends Node<GunNode>
{

	public var position:Position;
	public var gun:Gun;
	public function new() 
	{
		
	}
	
}