package com.sorochich.gameprototype.battle.entitysystem.nodes;
import ash.core.Node;
import com.sorochich.gameprototype.battle.entitysystem.components.Bullet;
import com.sorochich.gameprototype.battle.entitysystem.components.Health;
import com.sorochich.gameprototype.battle.entitysystem.components.Position;
import com.sorochich.gameprototype.battle.entitysystem.components.Speed;
import com.sorochich.gameprototype.battle.entitysystem.components.Target;

/**
 * ...
 * @author s_sorochich
 */
class BulletNode extends Node<BulletNode>
{

	public var target:Target;
	public var targetHealth:Health;
	public var position:Position;
	public var speed:Speed;
	public var bullet:Bullet;
	
	public function new() 
	{
		
	}
	
}