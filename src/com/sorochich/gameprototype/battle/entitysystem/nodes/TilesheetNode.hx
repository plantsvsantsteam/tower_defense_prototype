package com.sorochich.gameprototype.battle.entitysystem.nodes;

import ash.core.Node;
import com.sorochich.gameprototype.battle.entitysystem.components.Position;
import com.sorochich.gameprototype.battle.entitysystem.components.TilesheetAnimation;

/**
 * ...
 * @author s_sorochich
 */
class TilesheetNode extends Node<TilesheetNode>
{
	public var animation:TilesheetAnimation;
	public var position:Position;

	public function new() 
	{
		
	}
	
}