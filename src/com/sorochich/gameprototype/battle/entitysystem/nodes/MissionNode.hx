package com.sorochich.gameprototype.battle.entitysystem.nodes;

import ash.core.Node;
import com.sorochich.gameprototype.battle.entitysystem.components.Health;
import com.sorochich.gameprototype.battle.entitysystem.components.Mission;
import com.sorochich.gameprototype.battle.entitysystem.components.UI;

/**
 * ...
 * @author s_sorochich
 */
class MissionNode extends Node<MissionNode>
{
	public var mission:Mission;
	public var health:Health;
	public var ui:UI;

	public function new() 
	{
		
	}
	
}