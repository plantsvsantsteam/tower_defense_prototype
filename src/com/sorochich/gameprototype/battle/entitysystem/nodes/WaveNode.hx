package com.sorochich.gameprototype.battle.entitysystem.nodes;

import ash.core.Node;
import com.sorochich.gameprototype.battle.entitysystem.components.Wave;
import com.sorochich.gameprototype.battle.entitysystem.components.WaveMarker;

/**
 * ...
 * @author s_sorochich
 */
class WaveNode extends Node<WaveNode>
{
	public var wave:Wave;
	public var waveMarker:WaveMarker;
	
	public function new() 
	{
		
	}
	
}