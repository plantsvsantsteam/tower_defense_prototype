package com.sorochich.gameprototype.battle.entitysystem.nodes;

import ash.core.Node;
import com.sorochich.gameprototype.battle.entitysystem.components.Borders;
import com.sorochich.gameprototype.battle.entitysystem.components.Enemy;
import com.sorochich.gameprototype.battle.entitysystem.components.Health;
import com.sorochich.gameprototype.battle.entitysystem.components.PathRunner;
import com.sorochich.gameprototype.battle.entitysystem.components.Position;
import com.sorochich.gameprototype.battle.entitysystem.components.Reward;
import com.sorochich.gameprototype.battle.entitysystem.components.TilesheetAnimation;
import com.sorochich.gameprototype.battle.entitysystem.components.Wave;

/**
 * ...
 * @author s_sorochich
 */
class EnemyNode extends Node<EnemyNode>
{
	public var enemy:Enemy;
	public var wave:Wave;
	public var position:Position;
	public var borders:Borders;
	public var animation:TilesheetAnimation;

	public function new() 
	{
		
	}
	
}