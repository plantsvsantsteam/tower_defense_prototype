package com.sorochich.gameprototype.battle.entitysystem.nodes;

import ash.core.Node;
import com.sorochich.gameprototype.battle.entitysystem.components.PathRunner;
import com.sorochich.gameprototype.battle.entitysystem.components.Position;
import com.sorochich.gameprototype.battle.entitysystem.components.Speed;

/**
 * ...
 * @author s_sorochich
 */
class PathRunnerNode extends Node<PathRunnerNode>
{

	public var position:Position;
	public var speed:Speed;
	public var pathRunner:PathRunner;
	public function new() 
	{
		
	}
	
}