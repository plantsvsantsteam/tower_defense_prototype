package com.sorochich.gameprototype.battle.entitysystem.nodes;

import ash.core.Node;
import com.sorochich.gameprototype.battle.entitysystem.components.Display;
import com.sorochich.gameprototype.battle.entitysystem.components.Growth;
import com.sorochich.gameprototype.battle.entitysystem.components.Tower;

/**
 * ...
 * @author s_sorochich
 */
class GrowthNode extends Node<GrowthNode>
{
	public var display:Display;
	public var growth:Growth;
	public var tower:Tower;
	
	public function new() 
	{
		
	}
	
}