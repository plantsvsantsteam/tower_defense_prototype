package com.sorochich.gameprototype.battle.entitysystem.components;

/**
 * ...
 * @author s_sorochich
 */
class AttackGroup
{
	public var timeBeforeStart:Float;

	public var waveId:String;
	public var started:Bool = false;
	public var compeleted:Bool = false;
	public var wave:Wave;
	

	public function new(timeBeforeStart:Float, waveId:String) 
	{
		this.timeBeforeStart = timeBeforeStart;
		this.waveId = waveId;
	}
	
}