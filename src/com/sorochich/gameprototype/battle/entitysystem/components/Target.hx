package com.sorochich.gameprototype.battle.entitysystem.components;
import openfl.geom.Point;
import openfl.geom.Rectangle;

/**
 * ...
 * @author s_sorochich
 */
class Target
{
	public var position:Position;
	public var rect:Rectangle;

	//public var position:Point;
	public function new(position:Position,rect:Rectangle) 
	{
		this.position = position;
		this.rect = rect;
		
	}
	
}