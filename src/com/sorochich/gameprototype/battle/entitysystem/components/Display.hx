package com.sorochich.gameprototype.battle.entitysystem.components;
import openfl.display.DisplayObject;

/**
 * ...
 * @author s_sorochich
 */
class Display
{
	public var displayObject: DisplayObject;
	public var centrate: Bool;

	public function new(displayObject:DisplayObject, centrate: Bool=false) 
	{
		this.displayObject = displayObject;
		this.centrate = centrate;
	}
	
}