package com.sorochich.gameprototype.battle.entitysystem.components;
import openfl.geom.Point;

/**
 * ...
 * @author s_sorochich
 */
class PathRunner
{
	public var pathId:String;
	public var currentPointNum:Int = 0;
	public var currentPoint:Point; 
	public var currentSegment:Point; 
	public var currentSegmentLength:Float; 
	public var passedPath:Float; 
	public var pathComplete:Bool = false; 
	
	public function new(pathId:String) 
	{
		this.pathId = pathId;
	}
	
}