package com.sorochich.gameprototype.battle.entitysystem.components;
import com.sorochich.gameprototype.config.vo.MissionVO;

/**
 * ...
 * @author s_sorochich
 */
class Mission
{
	public var missionInfo:MissionVO;
	public var attacksNum:Int;
	public var currentAttackNum:Int;
	public var currentAttack:Attack;
	public var moneyValue:Int;
	
	public function new(missionInfo:MissionVO) 
	{
		this.missionInfo = missionInfo;
		attacksNum = missionInfo.attacks.length;
		moneyValue = missionInfo.startValue;
		currentAttackNum = 0;
	}
	
}