package com.sorochich.gameprototype.battle.entitysystem.components;
import ash.fsm.EntityStateMachine;

/**
 * ...
 * @author s_sorochich
 */
class Enemy
{

	public var stateMachine:EntityStateMachine;
	@:isVar
	private var state(get, set):String;
	public function new(stateMachine:EntityStateMachine) 
	{
		this.stateMachine = stateMachine;
	}
	
	public function get_state():String 
	{
		return state;
	}
	
	public function set_state(value:String):String 
	{
		stateMachine.changeState(value);
		return state = value;
	}
	
}