package com.sorochich.gameprototype.battle.entitysystem.components;
import com.sorochich.gameprototype.config.vo.GrowthAssetsVO;
import com.sorochich.gameprototype.config.vo.TowerVO;

/**
 * ...
 * @author s_sorochich
 */
class Growth
{
	public var resultVO:Dynamic;
	public var assetsVo:GrowthAssetsVO;

	public var growthTime:Float;
	public var timeLeft :Float;
	
	public function new(growthTime:Float, assetsVo:GrowthAssetsVO, resultVO:Dynamic) 
	{
		this.resultVO = resultVO;
		this.assetsVo = assetsVo;
		this.growthTime = timeLeft = growthTime;
		
	}
	
}