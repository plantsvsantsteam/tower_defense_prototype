package com.sorochich.gameprototype.battle.entitysystem.components;
import ash.fsm.EntityStateMachine;
import com.sorochich.gameprototype.config.vo.TowerVO;

/**
 * ...
 * @author s_sorochich
 */
class Tower
{

	public var vo:TowerVO;
	
	public var stateMachine:EntityStateMachine;
	public function new(stateMachine:EntityStateMachine) 
	{
		this.stateMachine = stateMachine;
	}
	
}