package com.sorochich.gameprototype.battle.entitysystem.components;

/**
 * ...
 * @author s_sorochich
 */
class Wave
{

	public var enemiesLeftToCreate:UInt;
	public var enemiesLeftInWave:UInt;
	public var timeBeforeNewEnemy:Float;
	public var currentTimeBeforeNewEnemy:Float=0;
	public var pathId:String;
	public var enemyId:String;
	public var complete:Bool = false;
	
	public function new(enemiesLeft:UInt, timeBeforeNewEnemy:Float, pathId:String, enemyId:String) 
	{
		this.timeBeforeNewEnemy = timeBeforeNewEnemy;
		this.enemiesLeftToCreate = this.enemiesLeftInWave = enemiesLeft;
		this.pathId = pathId;
		this.enemyId = enemyId;
		
	}
	
}