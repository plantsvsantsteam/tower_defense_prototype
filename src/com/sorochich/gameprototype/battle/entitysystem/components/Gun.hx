package com.sorochich.gameprototype.battle.entitysystem.components;

/**
 * ...
 * @author s_sorochich
 */
class Gun
{
	public var bulletId:String;
	public var distance:Float;
	public var totalReloadTime:Float;
	public var timeBeforeShot:Float;
	public var hasTarget:Bool;

	public function new(totalReloadTime:Float, distance:Float, bulletId:String) 
	{
		this.bulletId = bulletId;
		this.distance = distance;
		this.totalReloadTime = timeBeforeShot = totalReloadTime;
		
	}
	
}