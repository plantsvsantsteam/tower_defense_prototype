package com.sorochich.gameprototype.battle.entitysystem.components;
import com.sorochich.gameprototype.common.implementations.AtlasVO;

/**
 * ...
 * @author s_sorochich
 */
class TilesheetAnimation
{

	public var animation:Array<AtlasVO>;
	public var totalFrames:Int;
	public var currentFrame:Int = 0;
	public var framesBeforeUpdate:Int = 0;
	public var framesBeforeUpdateTotal:Int = 0;
	public var repeatable:Bool;
	public var complete:Bool = false;
	
	public function new(animation:Array<AtlasVO>,framesBeforeUpdateTotel:Int, curFrame:Int=0,repeatable:Bool=true) 
	{
		this.animation = animation;
		totalFrames = animation.length;
		this.framesBeforeUpdateTotal = framesBeforeUpdate = framesBeforeUpdateTotel;
		this.currentFrame = curFrame;
		this.repeatable = repeatable;
	}
	
}