package com.sorochich.gameprototype.battle;

import ash.core.Engine;
import ash.core.Entity;
import ash.tick.FrameTickProvider;
import com.sorochich.gameprototype.battle.entitysystem.EntityCreator;
import com.sorochich.gameprototype.battle.entitysystem.components.Display;
import com.sorochich.gameprototype.battle.entitysystem.components.Position;
import com.sorochich.gameprototype.battle.entitysystem.systems.AttackSystem;
import com.sorochich.gameprototype.battle.entitysystem.systems.BulletsSystem;
import com.sorochich.gameprototype.battle.entitysystem.systems.EnemySystem;
import com.sorochich.gameprototype.battle.entitysystem.systems.GrowthSystem;
import com.sorochich.gameprototype.battle.entitysystem.systems.GunsSystem;
import com.sorochich.gameprototype.battle.entitysystem.systems.MissionSystem;
import com.sorochich.gameprototype.battle.entitysystem.systems.PathRunningSystem;
import com.sorochich.gameprototype.battle.entitysystem.systems.RenderSystem;
import com.sorochich.gameprototype.battle.entitysystem.systems.TowersSystem;
import com.sorochich.gameprototype.battle.entitysystem.systems.WaveSystem;
import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.implementations.ScreenBase;
import com.sorochich.gameprototype.common.interfaces.IScreen;
import haxe.ui.toolkit.containers.Box;
import haxe.ui.toolkit.containers.HBox;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.interfaces.IDisplayObjectContainer;
import openfl.display.Sprite;
import openfl.display.Stage;
import openfl.events.Event;
import openfl.events.IEventDispatcher;
import openfl.events.MouseEvent;

/**
 * ...
 * @author s_sorochich
 */
class BattleScreen extends ScreenBase implements IScreen
{
	private var _ticker:FrameTickProvider;
	private var _engine:Engine;
	private var _container:Sprite;
	private var _spritesheetContainer:Sprite;
	private var _creator :EntityCreator;
	
	public function new(target:IEventDispatcher=null) 
	{
		super(target);
//
		//styledBox.visible = false;
		//panelsBox.visible = false;
		//buttonsBox.visible = false;
		
		_container = new Sprite();
		_spritesheetContainer = new Sprite();
		_engine = new Engine();
		_creator = new EntityCreator(_engine);
		_ticker = new FrameTickProvider(_container);
		
		_engine.addSystem(new RenderSystem(_container,_spritesheetContainer), 2);
		_engine.addSystem(new WaveSystem(_creator), 1);
		_engine.addSystem(new PathRunningSystem(), 1);
		_engine.addSystem(new EnemySystem(_creator), 1);
		_engine.addSystem(new AttackSystem(_creator), 1);
		_engine.addSystem(new MissionSystem(_creator,_container, _ticker), 1);
		_engine.addSystem(new GrowthSystem(), 1);
		_engine.addSystem(new TowersSystem(), 1);
		_engine.addSystem(new BulletsSystem(_creator), 1);
		_engine.addSystem(new GunsSystem(_creator), 1);
		
		ApplicationMain.instance.get_stage().addEventListener(Event.RESIZE, resize);
	}
	
	private function resize(e:Event):Void 
	{
		var stage:Stage = ApplicationMain.instance.get_stage();
		
		var ratioX: Float = stage.stageWidth / 1024;
		var ratioY: Float = stage.stageHeight / 768;
		var scale:Float = ratioX > ratioY?ratioY:ratioX;
		
		_container.scaleX = _container.scaleY = scale;
		_spritesheetContainer.scaleX = _spritesheetContainer.scaleY = scale;
		
		var offsetX:Float = (stage.stageWidth - _container.width) / 2;
		var offsetY:Float = (stage.stageHeight - _container.height) / 2;
		
		_container.x =_spritesheetContainer.x = offsetX;
		_container.y =_spritesheetContainer.y = offsetY;
	}
	
	override function onShow() 
	{
		super.onShow();
		ApplicationMain.instance.get_stage().addChild(_container);
		ApplicationMain.instance.get_stage().addChild(_spritesheetContainer);
		ApplicationMain.instance.get_stage().addChild(ApplicationMain.instance.get_uiRoot().sprite);


			
		_creator.createMission(ApplicationMain.instance.get_missionId());
		_ticker.timeAdjustment = 2;
		_ticker.add(_engine.update);
		
		_ticker.start();
		resize(null);
		//_ticker.stop();
	}
	
	private function click(e:MouseEvent):Void 
	{
		if(_ticker.playing){
			_ticker.stop();

		}else{
			_ticker.start();
		}
		
	}
	
	override function onHide() 
	{

		_container.addEventListener(Event.ENTER_FRAME, container_enterFrame);

		ApplicationMain.instance.get_stage().removeEventListener(MouseEvent.CLICK, click);
		
		ApplicationMain.instance.get_stage().removeChild(_spritesheetContainer);
		ApplicationMain.instance.get_stage().removeEventListener(Event.RESIZE, resize);

		_ticker.remove(_engine.update);
		_ticker.stop();
		trace("Battle Screen Ticker stop",_ticker.playing);

		super.onHide();
	}
	
	private function container_enterFrame(e:Event):Void 
	{
		ApplicationMain.instance.get_stage().removeChild(_container);
		_container.removeEventListener(Event.ENTER_FRAME, container_enterFrame);
		_engine.removeAllEntities();
		_engine.removeAllSystems();
		_engine = null;
	}
	
}