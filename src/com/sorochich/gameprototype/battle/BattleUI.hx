package com.sorochich.gameprototype.battle;
import com.sorochich.gameprototype.common.ApplicationMain;
import com.sorochich.gameprototype.common.events.ResourceEvent;
import com.sorochich.gameprototype.common.meta.I18n;
import com.sorochich.gameprototype.common.meta.Resources;
import haxe.ui.toolkit.containers.Box;
import haxe.ui.toolkit.containers.HBox;
import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.controls.Text;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.core.interfaces.IDisplayObjectContainer;
import openfl.events.MouseEvent;
import com.sorochich.gameprototype.common.const.Screen;
import com.sorochich.gameprototype.shop.ShopList;
/**
 * ...
 * @author s_sorochich
 */
class BattleUI
{
	var _currencyText:Text;
	public var _root:Root;
	public var _view:IDisplayObjectContainer;
	public var styledBox:Box;
	public var panelsBox:HBox;
	public var buttonsBox:VBox;
	public var hpText:Text;
	public var moneyText:Text;
	public var plantButton:Button;
	public var quitButton:Button;
	public var shopList:ShopList = new ShopList(true);
	public var shopBox:VBox;
	public function new() 
	{
		_root = ApplicationMain.instance.get_uiRoot();
		
		_view = Toolkit.processXmlResource("layouts/battleLayout.xml");
		_root.addChildAt(_view, 0);
		
		styledBox = _root.findChild("styledBox", Box, true);
		buttonsBox = _root.findChild("buttonsBox", VBox, true);
		quitButton = _root.findChild("quitButton", Button, true);
		plantButton = _root.findChild("plantButton", Button, true);
		hpText = _root.findChild("hpText", Text, true);
		moneyText = _root.findChild("moneyText", Text, true);
		
		
		shopBox = _root.findChild("myBox", VBox, true);
		
		shopBox.addChildAt(shopList, 0);
		
		hpText.style.fontSize = 20;
		hpText.style.color = 0xffffff;
		
		_currencyText = _root.findChild("currencyText", Text, true);
		
		_currencyText.style.color = 0xffffff;
		_currencyText.style.fontSize = 20;
		
		moneyText.style.color = 0xffffff;
		moneyText.style.fontSize = 20;
		
		quitButton.text = ApplicationMain.instance.get_textManager().getText(I18n.MENU_QUIT_BUTTON);
		plantButton.text = ApplicationMain.instance.get_textManager().getText( I18n.MENU_PLANT_BUTTON);
		_currencyText.text = Std.string(ApplicationMain.instance.get_profileManager().getResourceValue(Resources.RESEARCH_POINTS)) + " R.P.";
		ApplicationMain.instance.get_profileManager().addEventListener(ResourceEvent.RESOURCE_CHANGED, resourceChanged);
		quitButton.addEventListener(MouseEvent.CLICK, quitButton_click);
	}
	
	private function quitButton_click(e:MouseEvent):Void 
	{
		ApplicationMain.instance.get_screenManager().showScreen(Screen.PREBATTLE);
	}
	private function resourceChanged(e:ResourceEvent):Void 
	{
		if(e.resId == Resources.RESEARCH_POINTS){
			_currencyText.text = Std.string(e.newCount) + " R.P.";
		}
	}
	

	
	public function dispose() 
	{
		ApplicationMain.instance.get_profileManager().removeEventListener(ResourceEvent.RESOURCE_CHANGED, resourceChanged);

		//trace("DISPOSE");
		_root.removeChild(_view);
		_view.dispose();
		
	}
	
}