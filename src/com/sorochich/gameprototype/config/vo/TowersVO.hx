package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class TowersVO
{

	private var _towers : Array<TowerVO> = new Array<TowerVO>();

	public function new(xml:Fast) 
	{

		for (item in xml.nodes.tower){
			_towers.push(new TowerVO(item));
		}
		
	}
	
	public function getTowerById(id:String):TowerVO{
		for (e in _towers){
			if (e.id == id){
				return e;
			}
		}
		return null;
	}
	
}