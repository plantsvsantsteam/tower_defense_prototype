package com.sorochich.gameprototype.config.vo;
import com.sorochich.gameprototype.config.vo.MissionVO;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class MissionsVO
{

	private var _missions : Array<MissionVO> = new Array<MissionVO>();

	private var _ids:Array<String> = new Array<String>();
	public function new(xml:Fast) 
	{
		

		for (item in xml.nodes.mission){
			var vo = new MissionVO(item);
			_missions.push(vo);
			_ids.push(vo.id);
		}
		
	}
	
	public function getMissionById(id:String):MissionVO{
		for (m in _missions){
			if (m.id == id){
				return m;
			}
		}
		return null;
	}
	
	public function getListOfId():Array<String>{

		return _ids;
	}
	
}