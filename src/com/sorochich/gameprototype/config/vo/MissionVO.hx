package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class MissionVO
{

	public var attacks : Array<AttackVO> = new Array<AttackVO>();
	public var img:String;
	public var missionIcon:String;
	public var id:String;
	public var cellId:String;
	public var startValue:Int;
	public var baseHealth:Int;
	public function new(xml:Fast) 
	{
		id = xml.att.id;
		img = xml.att.img;
		missionIcon = xml.att.mission_icon;
		cellId = xml.att.cell_id;
		startValue = Std.parseInt(xml.att.start_value);
		baseHealth = Std.parseInt(xml.att.base_health);
		for (item in xml.nodes.attack){
			attacks.push(new AttackVO(item));
		}
		
	}
	
	
}