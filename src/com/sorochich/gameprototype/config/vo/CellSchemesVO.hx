package com.sorochich.gameprototype.config.vo;
import com.sorochich.gameprototype.config.vo.CellSchemeVO;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class CellSchemesVO
{

	private var _cells : Array<CellSchemeVO> = new Array<CellSchemeVO>();

	public function new(xml:Fast) 
	{

		for (item in xml.nodes.cell_scheme){
			_cells.push(new CellSchemeVO(item));
		}
		
	}
	
	public function getCellSchemeById(id:String):CellSchemeVO{
		for (e in _cells){
			if (e.id == id){
				return e;
			}
		}
		return null;
	}	
}