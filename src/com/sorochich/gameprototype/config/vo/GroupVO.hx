package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class GroupVO
{

	public var delay:Int;

	public var waveId:String;

	public function new(xml:Fast) 
	{
		delay = Std.parseInt(xml.att.delay);

		waveId = xml.att.waveId;
		
	}
	
}