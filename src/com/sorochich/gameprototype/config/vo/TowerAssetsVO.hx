package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class TowerAssetsVO
{

	
	public var active:String;
	public var damage_0:String;
	public var damage_1 : String;
	public var damage_2 : String;
	public var death : String;

	public var growth:GrowthAssetsVO = new GrowthAssetsVO();
	
	public function new(xml:Fast) 
	{

		//health : Std.parseInt(xml.att.health);

		
		//active : Std.parseInt(xml.att.growth_time);
		active = xml.att.active;
		damage_0 = xml.att.damage_0;
		damage_1 = xml.att.damage_1;
		damage_2 = xml.att.damage_2;
		death = xml.att.death;
		growth.growth_0 = xml.att.growth_0;
		growth.growth_1 = xml.att.growth_1;
		growth.growth_2 = xml.att.growth_2;

	}
	
}