package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class TowerVO
{

	//public var health:Int;
	//public var reward:Int;
	//public var speed:Int;
	//public var img:String;
	public var id:String;
	public var name:String;
	public var shopIcon:String;
	public var resourceId:String;
	public var growthTime:Int;
	public var price:Int;
	public var assets:TowerAssetsVO;
	public var gun:GunVO;
	
	public function new(xml:Fast) 
	{

		//health = Std.parseInt(xml.att.health);
		//reward = Std.parseInt(xml.att.reward);
		//speed = Std.parseInt(xml.att.speed);
		//img = xml.att.img;
		
		growthTime = Std.parseInt(xml.att.growth_time);
		price = Std.parseInt(xml.att.price);
		id = xml.att.id;
		resourceId = xml.att.resource_id;
		name = xml.att.name;
		shopIcon = xml.att.shop_icon;
		assets = new TowerAssetsVO( xml.nodes.assets.first());
		gun = new GunVO( xml.nodes.gun.first());
	}
	
}