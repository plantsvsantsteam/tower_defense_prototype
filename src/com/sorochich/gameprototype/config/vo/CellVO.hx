package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class CellVO
{

	public var x:Int;
	public var y:Int;

	
	public function new(xml:Fast) 
	{

		x = Std.parseInt(xml.att.x);
		y = Std.parseInt(xml.att.y);
	}
	
}