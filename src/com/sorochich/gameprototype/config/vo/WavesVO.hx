package com.sorochich.gameprototype.config.vo;
import com.sorochich.gameprototype.config.vo.WaveVO;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class WavesVO
{

	private var _waves : Array<WaveVO> = new Array<WaveVO>();

	public function new(xml:Fast) 
	{

		for (item in xml.nodes.wave){
			_waves.push(new WaveVO(item));
		}
		
	}
	
	public function getWaveById(id:String):WaveVO{
		for (w in _waves){
			if (w.id == id){
				return w;
			}
		}
		return null;
	}
	
}