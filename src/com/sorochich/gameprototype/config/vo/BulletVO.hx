package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class BulletVO
{

	public var id:String;
	public var img : String; 
	public var speed :Float;
	public var damage : Int;
	public function new(xml:Fast) 
	{
		id = xml.att.id;
		img = xml.att.img;
		speed = Std.parseFloat(xml.att.speed);
		damage = Std.parseInt(xml.att.damage);
		
	}
	
}