package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class CellSchemeVO
{

	public var cells : Array<CellVO> = new Array<CellVO>();
	public var id:String;

	public function new(xml:Fast) 
	{
		id = xml.att.id;
		for (item in xml.nodes.cell){
			cells.push(new CellVO(item));
		}
		
	}
	
}