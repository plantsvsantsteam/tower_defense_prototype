package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class BulletsVO
{

	private var _bullets : Array<BulletVO> = new Array<BulletVO>();

	public function new(xml:Fast) 
	{

		for (item in xml.nodes.bullet){
			_bullets.push(new BulletVO(item));
		}
		
	}
	
	public function getBulletById(id:String):BulletVO{
		for (e in _bullets){
			if (e.id == id){
				return e;
			}
		}
		return null;
	}
	
}