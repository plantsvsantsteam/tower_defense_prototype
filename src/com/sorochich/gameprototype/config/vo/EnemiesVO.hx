package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;
import com.sorochich.gameprototype.config.vo.EnemyVO;

/**
 * ...
 * @author s_sorochich
 */
class EnemiesVO
{
	private var _enemies : Array<EnemyVO> = new Array<EnemyVO>();

	public function new(xml:Fast) 
	{

		for (item in xml.nodes.enemy){
			_enemies.push(new EnemyVO(item));
		}
		
	}
	
	public function getEnemyById(id:String):EnemyVO{
		for (e in _enemies){
			if (e.id == id){
				return e;
			}
		}
		return null;
	}
	
}