package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class WaveVO
{
	
	public var count:Int;
	public var interval:Int;
	public var enemyId:String;
	public var pathId:String;
	public var id:String;

	public function new(xml:Fast) 
	{
		count = Std.parseInt(xml.att.count);
		interval = Std.parseInt(xml.att.interval);

		enemyId = xml.att.enemyId;
		pathId = xml.att.pathId;
		id = xml.att.id;
		
	}
	
}