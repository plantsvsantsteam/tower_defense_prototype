package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class GunVO
{

	public var reloadingTime:Float;
	public var distance:Float;
	public var bulletId:String;
	public function new(xml:Fast) 
	{
		reloadingTime = Std.parseFloat(xml.att.reloading_time);
		distance = Std.parseFloat(xml.att.distance);
		bulletId = xml.att.bullet_id;
		
	}
	
}