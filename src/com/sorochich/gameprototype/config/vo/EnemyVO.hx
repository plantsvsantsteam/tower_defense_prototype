package com.sorochich.gameprototype.config.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class EnemyVO
{

	public var health:Int;
	public var reward:Int;
	public var speed:Int;
	public var updateFrame:Int;
	public var imgRun:String;
	public var imgDead:String;
	public var id:String;
	
	public function new(xml:Fast) 
	{

		health = Std.parseInt(xml.att.health);
		reward = Std.parseInt(xml.att.reward);
		speed = Std.parseInt(xml.att.speed);
		updateFrame = Std.parseInt(xml.att.update_frame);
		imgRun = xml.att.img_run;
		imgDead = xml.att.img_dead;
		id = xml.att.id;
	}
	
}