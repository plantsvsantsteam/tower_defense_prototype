package com.sorochich.gameprototype.config.vo;
import com.sorochich.gameprototype.config.vo.GroupVO;
import haxe.xml.Fast;

/**
 * ...
 * @author s_sorochich
 */
class AttackVO
{

	public var groups : Array<GroupVO> = new Array<GroupVO>();

	public function new(xml:Fast) 
	{

		for (item in xml.nodes.group){
			groups.push(new GroupVO(item));
		}
		
	}
	
	
}