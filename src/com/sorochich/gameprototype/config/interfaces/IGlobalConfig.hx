package com.sorochich.gameprototype.config.interfaces;
import com.sorochich.gameprototype.config.vo.BulletVO;
import com.sorochich.gameprototype.config.vo.CellSchemeVO;
import com.sorochich.gameprototype.config.vo.MissionVO;
import com.sorochich.gameprototype.config.vo.TowerVO;
import com.sorochich.gameprototype.config.vo.WaveVO;
import openfl.events.IEventDispatcher;
import openfl.geom.Point;
import com.sorochich.gameprototype.config.vo.EnemyVO;

/**
 * @author s_sorochich
 */
interface IGlobalConfig extends IEventDispatcher 
{
  public function getEnemyById(id:String):EnemyVO;
  
  public function getPathPoint(pathId:String, pointNum:Int):Point;
  
  public function getWaveById(id:String):WaveVO;
  
  public function getMissionById(id:String):MissionVO;
  
  public function getCellSchemeById(id:String):CellSchemeVO;
  
  public function getTowerById(id:String):TowerVO;
  
  public function getBulletById(id:String):BulletVO;
  public function getListOfMissionIds():Array<String>;
}