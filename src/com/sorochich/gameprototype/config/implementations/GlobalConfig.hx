package com.sorochich.gameprototype.config.implementations;

import com.sorochich.gameprototype.config.vo.BulletVO;
import com.sorochich.gameprototype.config.vo.BulletsVO;
import com.sorochich.gameprototype.config.vo.CellSchemeVO;
import com.sorochich.gameprototype.config.vo.CellSchemesVO;
import com.sorochich.gameprototype.config.vo.MissionVO;
import com.sorochich.gameprototype.config.vo.MissionsVO;
import com.sorochich.gameprototype.config.vo.Paths;
import com.sorochich.gameprototype.config.vo.TowerVO;
import com.sorochich.gameprototype.config.vo.TowersVO;
import com.sorochich.gameprototype.config.vo.WaveVO;
import com.sorochich.gameprototype.config.vo.WavesVO;
import haxe.xml.Fast;
import openfl.Assets;
import openfl.events.EventDispatcher;
import openfl.events.IEventDispatcher;
import openfl.geom.Point;
import com.sorochich.gameprototype.config.interfaces.IGlobalConfig;
import com.sorochich.gameprototype.config.vo.EnemiesVO;
import com.sorochich.gameprototype.config.vo.EnemyVO;

/**
 * ...
 * @author s_sorochich
 */
class GlobalConfig extends EventDispatcher implements IGlobalConfig
{

	private var _enemies:EnemiesVO;
	private var _paths:Paths;
	private var _waves:WavesVO;
	private var _missions:MissionsVO;
	private var _cellSchemes:CellSchemesVO;
	private var _towers:TowersVO;
	private var _bullets:BulletsVO;
	
	
	public function new(target:IEventDispatcher=null) 
	{
		super(target);
		_paths = new Paths();
		_enemies = new EnemiesVO(new Fast(Xml.parse(Assets.getText("configs/enemies.xml")).firstElement()));
		_waves = new WavesVO(new Fast(Xml.parse(Assets.getText("configs/waves.xml")).firstElement()));
		_missions = new MissionsVO(new Fast(Xml.parse(Assets.getText("configs/missions.xml")).firstElement()));
		_cellSchemes = new CellSchemesVO(new Fast(Xml.parse(Assets.getText("configs/cells.xml")).firstElement()));
		_towers = new TowersVO(new Fast(Xml.parse(Assets.getText("configs/towers.xml")).firstElement()));
		_bullets = new BulletsVO(new Fast(Xml.parse(Assets.getText("configs/bullets.xml")).firstElement()));
		
		//trace(_paths.getPathPoint('1', 0));
	}
	
	
	/* INTERFACE src.config.interfaces.IGlobalConfig */
	
	public function getEnemyById(id:String):EnemyVO 
	{
		return _enemies.getEnemyById(id);
	}
	
	
	/* INTERFACE src.config.interfaces.IGlobalConfig */
	
	public function getPathPoint(pathId:String, pointNum:Int):Point 
	{
		return _paths.getPathPoint(pathId, pointNum);
	}
	
	
	/* INTERFACE src.config.interfaces.IGlobalConfig */
	
	public function getWaveById(id:String):WaveVO 
	{
		return _waves.getWaveById(id);
	}
	
	
	/* INTERFACE src.config.interfaces.IGlobalConfig */
	
	public function getMissionById(id:String):MissionVO 
	{
		return _missions.getMissionById(id);
	}
	
	
	/* INTERFACE src.config.interfaces.IGlobalConfig */
	
	public function getCellSchemeById(id:String):CellSchemeVO 
	{
		return _cellSchemes.getCellSchemeById(id);
	}
	
	
	/* INTERFACE src.config.interfaces.IGlobalConfig */
	
	public function getTowerById(id:String):TowerVO 
	{
		return _towers.getTowerById(id);
	}
	
	
	/* INTERFACE src.config.interfaces.IGlobalConfig */
	
	public function getBulletById(id:String):BulletVO 
	{
		return _bullets.getBulletById(id);
	}
	
	
	/* INTERFACE src.config.interfaces.IGlobalConfig */
	
	public function getListOfMissionIds():Array<String> 
	{
		return _missions.getListOfId();
	}
	
}